//
//  StopWatch.m
//  stopwatch
//
//  Created by DEEPINDERPAL SINGH on 25/07/17.
//  Copyright © 2017 Kevin Kelly. All rights reserved.
//

#import "StopWatch.h"



@implementation StopWatch

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)startPressed:(id)sender {
    start_time = [NSDate date];
    if (clockTicks == nil) {
        clockTicks = [NSTimer scheduledTimerWithTimeInterval:1.0/100.0
                                                      target:self
                                                    selector:@selector(updateTimer)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}

- (void)stopPressed:(id)sender {
    [clockTicks invalidate];
    clockTicks = nil;
}

- (void)pausePressed:(id)sender {
    pause_time = current_time;
}

- (void)resumePressed:(id)sender {
    NSTimeInterval differance = [current_time timeIntervalSinceDate:pause_time];
    start_time = [start_time dateByAddingTimeInterval:differance];
    NSLog(@"%@",start_time);
    if (clockTicks == nil) {
        clockTicks = [NSTimer scheduledTimerWithTimeInterval:1.0/100.0
                                                      target:self
                                                    selector:@selector(updateTimer)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}

-(void)updateTimer{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:start_time];
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"m.ss.SSS"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    NSString *timeString=[dateFormatter stringFromDate:timerDate];
    current_time = timerDate;
    [self.delegate updateLabel:timeString];
}

@end
