//
//  JobDetailCreatorViewController.swift
//  cmm
//
//  Created by David Lakhi on 08/03/18.
//  Copyright © 2018 Custom Music Market. All rights reserved.
//

import UIKit

class JobDetailCreatorViewController: UIViewController {
    
    var postId: String = ""
    
    @IBOutlet var jobTitle: UILabel!
    @IBOutlet var jobDesc: UITextView!
    @IBOutlet var proposalText: UITextView!
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var jobStatus: UILabel!
    @IBOutlet var completeBtn: UIButton!
    @IBOutlet var chatBtn: UIButton!
    
    var userInfo:[String:Any] = [:]
    var jobPostDetail: [String : AnyObject]?
    var userInfoNil: [String : AnyObject]?
    var downloadTask: URLSessionDownloadTask?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(userInfo)
        
        var price:String = ""
        var status:String = ""
        
        if Global.sharedInstance.user_role == "creator" {
            price = userInfo["budget_amount"] as! String
            status = userInfo["status"] as! String
            priceLbl.text = "$ " + price
            jobStatus.text = status
            jobTitle.text = (userInfo["post_title"] as? String)?.capitalized
            jobDesc.text = userInfo["description"] as? String
            proposalText.text = userInfo["proposal_text"] as? String
        } else {
            price = userInfo["budget"] as! String
            status = userInfo["status"] as! String
            priceLbl.text = "$ " + price
            jobStatus.text = status.capitalized
            jobTitle.text = (userInfo["title"] as? String)?.capitalized
            jobDesc.text = userInfo["description"] as? String
            proposalText.text = userInfo["proposal_text"] as? String
        }
        chatBtn.isHidden = true
        completeBtn.isHidden = true
        
        if status == "hire"  {
            chatBtn.isHidden = false
            completeBtn.isHidden = false
        } else if status == "cancelled" {
            chatBtn.isHidden = false
        } else if status == "completed" {
            chatBtn.isHidden = false
        } else if status == "submitted" {
            chatBtn.isHidden = false
            completeBtn.isHidden = false
        }
        
        //else if status == "post" {
        //    chatBtn.isHidden = false
        //}
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func selected(sender: AnyObject) {
        
        let postId = userInfo["post_id"] as! String
        let consumerId = userInfo["consumer_id"] as! String
        let thread_id = userInfo["thread_id"] as! String
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MessageViewControllerID") as! MessageViewController
        controller.postId = postId
        controller.receiver = consumerId
        controller.thread_id = thread_id
        print("Post-ID = ",postId, "Consumer-ID = ", consumerId)
        
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func completed(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "JobCompletionViewControllerID") as! JobCompletionViewController
        controller.post_id = userInfo["post_id"] as! String
        present(controller, animated: true, completion: nil)
    }
    
    @IBOutlet weak var cancelJob: UIButton!
    
}
