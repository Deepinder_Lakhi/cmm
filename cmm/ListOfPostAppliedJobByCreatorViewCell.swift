//
//  ListOfPostAppliedJobByCreatorViewCell.swift
//  MMC
//
//  Created by Prabhat on 05/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class ListOfPostAppliedJobByCreatorViewCell: UITableViewCell {
    @IBOutlet var userImg: UIImageView!
    @IBOutlet var userDesc: UILabel!
    @IBOutlet var userName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImg.layer.borderWidth = 1
        userImg.layer.masksToBounds = false
        userImg.layer.borderColor = UIColor.white.cgColor
        userImg.layer.cornerRadius = userImg.frame.height/2
        userImg.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
