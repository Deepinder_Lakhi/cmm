//
//  ForgotViewController.swift
//  
//
//  Created by DEEPINDERPAL SINGH on 28/07/17.
//
//

import UIKit

class ForgotViewController: UIViewController {

    
    @IBOutlet weak var txtEmail: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonPressed() {
        navigationController?.pop(true)
    }
    
    @IBAction func forgetPasswordBtnPressed() {
        if (txtEmail.text!.isEmpty) {
            alertMessage(title: "Alert", message: "Please make sure that field is not empty.!")
        } else {
            forgetPasswordRequest()
        }
    }
    
    func alertMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Ok")
        })
        alert.addAction(dismissButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func forgetPasswordRequest()
    {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        
        let urlStr:String = "\(baseUrl)users_forgot_password.php"
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let postString = "user_email=\(txtEmail.text!)"
        
        request.httpBody = postString.data(using: .utf8)
        
        print(postString)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            var json = try? JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json as Any)
            
            let success = json?["Success"] as! String
            
            DispatchQueue.main.async {
                if success == "1" {
                    
                    var path = json!["url"] as! String
                    if ((json?["id"]) != nil) {
                        path.append("?id=\(json?["id"] as! String)")
                        print(path)
                        UIApplication.shared.openURL(URL(string: path)!)
                    }
                }
                Global.sharedInstance.hideLoader()
            }
        }
        

        
        task.resume()
    }
}
