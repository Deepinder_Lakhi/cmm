//
//  CreatorsListsCell.swift
//  MMC
//
//  Created by Prabhat on 20/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class CreatorsListsCell: UITableViewCell {

    @IBOutlet var name: UILabel!
    @IBOutlet var detailBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
