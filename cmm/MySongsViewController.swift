//
//  MySongsViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 20/07/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import CoreAudio

class MySongsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var allPosts:Array<Any> = []
    var stations = [RadioStation]()
    var currentStation: RadioStation?
    var currentTrack: Track?
    let radioPlayer = Player.radio
    var track: Track!
    var currentTag:Int?
    var downloadTask: URLSessionDownloadTask?
    
    var player: AVPlayer!
    var observer: Any!


    @IBOutlet weak var tableView: UITableView!

    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fetchStorePost()
    }
       
    override func viewWillLayoutSubviews() {
        
    }
    
    @IBAction func shareButtonClicked(sender: UIButton) {
        let station = stations[sender.tag]
        let shareTitle = "Check out this wonderfull song \(station.musicsTitleTrack as String)"
        let songUrl = station.musicsMusicFile as String
        let objectsToShare = [shareTitle, songUrl] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = sender
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    func fetchStorePost() {
        // Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("consumer_list_songs_by_uploading_creators.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")
        let postString = "consumer_id=\(userID!)"
        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            print(jsonData!)
            
            let fullData = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            
            let json = JSON(data: data as Data)
            print(json)
            
            let success  = fullData["Success"] as AnyObject
            print(success)
            self.stations = [RadioStation]()
            if success.boolValue == true {
                self.allPosts = json["music_history"].array!
                print(self.allPosts)
                for stationJSON in self.allPosts {
                    print("Data to copy: \(stationJSON)")
                    let station = RadioStation.parseMyMusic(stationJSON: stationJSON as! JSON)
                    self.stations.append(station)
                }
                print(self.stations)
                
            }

            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
            }
        }
        
        task.resume()
    }


    // number of rows in table view
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allPosts.count > 0 {
            
            //self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 44, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No songs Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stations.count
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "mySongs", for: indexPath) as! MySongCell
        
        let station = stations[indexPath.row]
        cell.songTitle.text = station.musicsTitleTrack
        cell.songType.titleLabel?.text = " Music Type: \(station.musicsType)"
        cell.SongDur.titleLabel?.text = " Duration: \(station.musicsDuration)"
        cell.playButton.tag = indexPath.row
        cell.shareButton.tag = indexPath.row
        
        let imageURL = station.musicsImage as String
        
        if imageURL.contains("http") {
            
            if let url = URL(string: station.musicsImage) {
                downloadTask = cell.userImg.loadImageWithURL(url: url) { (image) in
                    // station image loaded
                    cell.userImg.image = image
                }
            }
            
        } else if imageURL != "" {
            cell.userImg.image = UIImage(named: imageURL as String)
        } else {
            cell.userImg.image = UIImage(named: "stationImage")
        }
        radioPlayer.stop()
        if indexPath.row == currentTag {
            let musicURL = station.musicsMusicFile as String
            print(musicURL)
            cell.playButton.setImage(UIImage(named: "icon-pause"), for: UIControlState.normal)
            self.playSong(url: URL(string:musicURL)!, isBoundryNeeded: false)
        } else {
            cell.playButton.setImage(UIImage(named: "icon-play"), for: UIControlState.normal)
        }

        return cell
    }
   
    
    func playSong(url:URL, isBoundryNeeded:Bool) {
        player = AVPlayer(url: url)
        player.play()
        if isBoundryNeeded == true{
            let boundary: TimeInterval = 10
            let times = [NSValue(time: CMTimeMake(Int64(boundary), 1))]
            observer = player.addBoundaryTimeObserver(forTimes: times, queue: nil) {
                [weak self] time in
                print("10s reached")
                if let observer = self?.observer {
                    self?.player.removeTimeObserver(observer)
                }
                self?.resetPlayer()
            }
        }
    }
    
    func resetPlayer() {
        //Here we are reserting the player for the next use
        player.pause()
        self.currentTag = self.stations.count + 1
        self.tableView.reloadData()
    }
    
    func resumePlayer() {
        //Here we are reserting the player for the next use
        player.play()
        self.currentTag = self.stations.count + 1
        self.tableView.reloadData()
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }

}
