//
//  ProfileViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 24/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserProfileVC()
        
        Global.sharedInstance.showLoaderWithMsg((self.view)!)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.removeLoader),
            name: NSNotification.Name(rawValue: "RemoveLoader"),
            object: nil)
        

        // Do any additional setup after loading the view.
    }
    
    func removeLoader() {
        Global.sharedInstance.hideLoader()
    }
    
    @IBAction func showActionSheet() {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "Logout", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            Global.sharedInstance.logOutFromApp()
            self.launchLoginScreen()
            print("Saved")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func moveToRootView() {
        let rootVC = storyboard!.instantiateViewController(withIdentifier: "LaunchViewControllerID") as! LaunchViewController
        
        UIApplication.shared.keyWindow?.rootViewController = rootVC
    }
    
    func launchLoginScreen() {
        let rootVC = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "LaunchViewControllerID") as! LaunchViewController
        let nvc:UINavigationController = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "main") as! UINavigationController
        nvc.viewControllers = [rootVC]
        UIApplication.shared.keyWindow?.rootViewController = nvc
    }

    
    func loadUserProfileVC() {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileViewControllerID") as! UserProfileViewController
        self.addChildViewController(viewController)
        viewController.view.frame = containerView.bounds
        containerView.addSubview(viewController.view)
    }
    

}
