//
//  WantToBeViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 09/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class WantToBeViewController: UIViewController {
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.pop(true)
    }
    
    @IBAction func continueAs(_ sender: Any) {
        let new = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewControllerID") as! SignupViewController
        if (sender as AnyObject).tag == 10 {
            Global.sharedInstance.user_role = "creator"
        } else {
            Global.sharedInstance.user_role = "consumer"
        }
        self.navigationController?.pushViewController(new, animated: true)
    }
}
