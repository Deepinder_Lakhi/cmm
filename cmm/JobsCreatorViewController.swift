//
//  JobsCreatorViewController.swift
//  MMC
//
//  Created by Prabhat on 30/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class JobsCreatorViewController: ButtonBarPagerTabStripViewController, UISearchBarDelegate {

    @IBOutlet var searchbar: UISearchBar!
    
    
    let currentTab = #colorLiteral(red: 0.2751877904, green: 0.6797052026, blue: 0.9078940153, alpha: 1)
    let barBGColor = #colorLiteral(red: 0.8979603648, green: 0.8980897069, blue: 0.8979320526, alpha: 1)
    let barTextColor = #colorLiteral(red: 0.4078193307, green: 0.4078193307, blue: 0.4078193307, alpha: 1)
    
    override func viewDidLoad() {
        
        // change selected bar color
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = currentTab
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 17)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = {
            [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = self?.barTextColor
            newCell?.label.textColor = self?.currentTab
        }
        
        searchbar.showsCancelButton = true
        searchbar.delegate = self
        
        super.viewDidLoad()
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let postVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedJobViewControllerID")
        let allVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InviteJobViewControllerID")
        let activeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ActiveJobCreatorViewControllerID")
        return [postVC, allVC, activeVC]
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        // Remove focus from the search bar.
        searchBar.endEditing(true)
    }

    
    override func viewDidDisappear(_ animated: Bool) {
        searchbar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.endEditing(true)
    }
 */
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        //write other necessary statements
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        
        // Remove focus from the search bar.
        searchBar.endEditing(true)
    }

}
