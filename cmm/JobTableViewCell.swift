//
//  JobTableViewCell.swift
//  MMC
//
//  Created by Prabhat on 28/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class JobTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var songTitle: UILabel!
    @IBOutlet weak var songContent: UILabel!
    @IBOutlet weak var budgetPrice: UILabel!
    @IBOutlet weak var songLength: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var timeAgo: UILabel!
    @IBOutlet weak var jobStatus: DLButtonExtender!
    @IBOutlet weak var fullView: UIButton!
    @IBOutlet weak var moreView: UIView!
    @IBOutlet weak var moreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
