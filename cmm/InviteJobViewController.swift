//
//  InviteJobViewController.swift
//  MMC
//
//  Created by Prabhat on 30/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class InviteJobViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    var allInvitedJob:Array<Any> = []

    // don't forget to hook this up from the storyboard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        // Custom Table View Cell Identifier
        tableView.register(UINib(nibName:"JobTableViewCell", bundle: nil), forCellReuseIdentifier: "JobTableViewID")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewWillLayoutSubviews() {
        if allInvitedJob.isEmpty {
            getAllInvitedJob()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismissKeyboard()
    }
    
    /* Helper Method To GET The Data On Server */
    func getAllInvitedJob() {
        var urlStr:String = baseUrl
        urlStr.append("creator_inviataions_list.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")
        let postString = "creator_id=\(userID!)"
        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                self.removeLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                self.removeLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            print(jsonData!)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            let success  = json["Success"] as AnyObject
            
            if success as! String == "1" {
                
                
                  
                    self.allInvitedJob = (json["invites_job_lists"] as! NSArray) as! Array<Any>
                

                
                
                print(self.allInvitedJob)
            }
            
            DispatchQueue.main.async {
                self.removeLoader()
                self.tableView.reloadData()
            }
        }
        
        task.resume()
    }
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "INVITES")
    }

    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allInvitedJob.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         removeLoader()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobTableViewID", for: indexPath) as! JobTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Configure the cell...
        let jobData = allInvitedJob[indexPath.row] as! [String: Any]
       // cell.jobStatus.tag = indexPath.row
        
   
        cell.songTitle.text = jobData["title"] as? String
        cell.songContent.text = jobData["song_content"] as? String
        
        let g = jobData["genre"] as? String
        
        cell.genre.text = "Genre:- " + g!
        let price = jobData["budget"] as? String
        cell.budgetPrice.text = "$ " + price!
        
        let length = jobData["length"] as? String
        
        cell.songLength.text = "Length:- " + length!
        
        let s = jobData["Inviations"] as? String
        
        
        cell.timeAgo.text = "Status:- " + (s?.uppercaseFirst)!
        
        
        cell.jobStatus.addTarget(self, action: #selector(FeedJobViewController.selected(sender:)), for: .touchUpInside)
        
        let postId = jobData["post_id"] as? String
        
        cell.jobStatus.tag = Int(postId!)!
        
        return cell
    }
    
    func selected(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ApplyNowViewControllerID") as! ApplyNowViewController
        controller.applyId = String(sender.tag)
        
        present(controller, animated: true, completion: nil)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allInvitedJob.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Job Invitation"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
    }
    
    func removeLoader() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveLoader"), object: nil, userInfo: nil)
    }
}
