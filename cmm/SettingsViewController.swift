//
//  SettingsViewController.swift
//  MMC
//
//  Created by Maninder on 17/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var size = self.view.frame.size
        
        size.height = 1000
        
        self.scrollView.contentSize = size

        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.pop(true)
    }

}
