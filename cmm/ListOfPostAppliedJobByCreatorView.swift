//
//  ListOfPostAppliedJobByCreatorView.swift
//  MMC
//
//  Created by Prabhat on 05/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit


class ListOfPostAppliedJobByCreatorView: UIViewController, UITableViewDelegate, UITableViewDataSource, appliedJobDelegate {
    
    var postId: String = ""
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var jobTitle: UILabel!
    @IBOutlet var jobDesc: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var jobStatus: UILabel!

    
    var userInfo:Array<Any> = []
    var jobPostDetail: [String : AnyObject]?
    var userInfoNil: [String : AnyObject]?
    var downloadTask: URLSessionDownloadTask?


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(postId)
        postJob()
    }
    
    func autoDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    /* Helper Method To POST The Data On Server */
    func postJob() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("/consumer_post_details_creator.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let postString = "post_id=\(postId)"
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
             print(json)
            
            self.userInfoNil = json["creator info"] as? [String : Any] as [String : AnyObject]?
            
            if self.userInfoNil == nil {
                if let data = (json["creator info"] as? NSArray) as? Array<Any> {
                    self.userInfo = data
                    print(self.userInfo)
                }
            }
            
            self.jobPostDetail = json["post details"] as? [String : Any] as [String : AnyObject]?
            
            print(self.jobPostDetail as Any)
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.tableView.reloadData()
                self.loadJobData()
            }
        }
        
        task.resume()
    }
    
    func loadJobData() -> Void {
        
        self.jobTitle.text = self.jobPostDetail?["post name"] as? String
        self.jobDesc.text = self.jobPostDetail?["post description"] as? String
        let price = self.jobPostDetail?["price"] as? String
        self.price.text = "$ " + price!
        let status = self.jobPostDetail?["status"] as? String
        self.jobStatus.text = status?.uppercaseFirst
    }



    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if userInfo.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Apply Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListOfPostAppliedJobByCreatorViewCellID", for: indexPath) as! ListOfPostAppliedJobByCreatorViewCell
        
        let cellData = userInfo[indexPath.row] as! [String: Any]
        
        cell.userName.text = cellData["user_name"] as? String
       // cell.userDesc.text = cellData["description"] as? String
        // cell.userDesc.text = "it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,"

        /*
        let jobsPrice = userInfo[indexPath.row] as! [String: Any]
        let imageURL = jobsPrice["user_profile_image"] as? String
        
        
        if (imageURL?.contains("http"))! {
            
            if let url = URL(string: imageURL!) {
                downloadTask = cell.userImg.loadImageWithURL(url: url) { (image) in
                    // station image loaded
                }
            }
            
        } else if imageURL != "" {
            cell.userImg.image = UIImage(named: imageURL!)
        } else {
            cell.userImg.image = UIImage(named: "stationImage")
        }
        */
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = userInfo[indexPath.row] as! [String: Any]
        let postId = jobPostDetail?["Post id"]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AppliedJobCreatorInfoViewControllerID") as! AppliedJobCreatorInfoViewController
        print(user)
        controller.delegate = self
        controller.postId = postId as! String
        controller.UserInfo = user
        present(controller, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
