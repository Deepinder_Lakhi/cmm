//
//  WebShellViewController.swift
//  MMC
//
//  Created by David on 5/04/18.
//  Copyright © 2018 Digiwolves. All rights reserved.
//

import UIKit

class WebShellViewController: UIViewController, UIWebViewDelegate {

    var urlString = String()
    var myTitle = String()
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        Global.sharedInstance.showLoaderWithMsg(self.view)
        titleLbl.text = myTitle
        print(urlString)
        loadWebWithHtml(urlString)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.pop(true)
    }

    override public var prefersStatusBarHidden : Bool {
        return false
    }
    
    func loadWebWithHtml(_ urlString: String)  {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Global.sharedInstance.hideLoader()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Global.sharedInstance.hideLoader()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Global.sharedInstance.hideLoader()
    }


}
