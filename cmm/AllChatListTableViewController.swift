//
//  AllChatListTableViewController.swift
//  MMC
//
//  Created by Prabhat on 25/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class AllChatListTableViewController: UITableViewController {
    
    var allUser: Array<Any> = []
    var downloadTask: URLSessionDownloadTask?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUserProfileData()
    }
    
    /* Helper Method To POST The Data On Server */
    func getUserProfileData() -> Void {
        var urlStr:String = baseUrl
        urlStr.append("chating_list_info.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")!

        let postString = "user_id=\(userID)"
        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                self.removeLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                self.removeLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            let success = json["Success"] as? String
            if success! == "1" {
                
//                if Global.sharedInstance.user_role == "consumer" {
                    self.allUser = (json["chat_listing"] as! NSArray) as! Array<Any>
//                }
                
                
                print(self.allUser )
            }
            
            DispatchQueue.main.async {
                self.removeLoader()
                self.tableView.reloadData()
            }
        }
        task.resume()
    }
    
    func removeLoader() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveLoader"), object: nil, userInfo: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allUser.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Chat Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        print(self.allUser.count)
        return self.allUser.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllMessageCellID", for: indexPath) as! AllMessageCell

        // Configure the cell...
        let userData = allUser[indexPath.row] as! [String: Any]
        
        let name = userData["name"] as? String
//        let imageURL = userData["profile_image"] as? String
//        
//        if (imageURL?.contains("http"))! {
//            
//            if let url = URL(string: imageURL) {
//                downloadTask = cell.thumbImageView.loadImageWithURL(url: url) { (image) in
//                    // station image loaded
//                }
//            }
//            
//        } else if imageURL != "" {
//            cell.thumbImageView.image = UIImage(named: imageURL as String)
//        } else {
//            cell.thumbImageView.image = UIImage(named: "stationImage")
//        }

        let array: NSMutableArray = []
        let string  = name?.components(separatedBy: " ")
        for word in string! {
            array.add(word)
        }
        if array.count == 2 {
            let firstName:String? = array[0] as? String
            let lastName:String? = array[1] as? String
            let fullname = (firstName?.first.uppercaseFirst)! + (lastName?.first.uppercaseFirst)!
            cell.userImg.text = fullname
        } else if (array.count == 1) {
            let firstName:String? = array[0] as? String
            let fullname = (firstName?.first.uppercaseFirst)!
            cell.userImg.text = fullname
        }
        else if (array.count == 3) {
            let firstName:String? = array[0] as? String
           // let muddleName:String? = array[1] as? String
            let lastName:String? = array[2] as? String
            let fullname = (firstName?.first.uppercaseFirst)! + (lastName?.first.uppercaseFirst)!
            cell.userImg.text = fullname
        }
        
        cell.userName.text = name?.uppercaseFirst
        
        let desc = userData["post_title"] as? String
        cell.messageDesc.text = desc?.uppercaseFirst
        
        let imgUrl = userData["name"] as? String

        let fileUrl = Foundation.URL(string: imgUrl!)
        if fileUrl != nil {
            print(fileUrl!)
        }
    
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userData = self.allUser[indexPath.row] as! [String: Any]
        print(userData)
        let senderID = userData["receiver"] as! String
        let postID = userData["post_id"] as! String
        let thread_id = userData["thread_id"] as! String
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MessageViewControllerID") as! MessageViewController
        controller.postId = postID
        controller.receiver = senderID
        controller.thread_id = thread_id
        print(" Post ID = ",postID)
        print(" Receiver ID = ",senderID)
        present(controller, animated: true, completion: nil)
    }

}
