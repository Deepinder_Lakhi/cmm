//
//  RadioStation.swift
//  Swift Radio
//
//  Created by Matthew Fecher on 7/4/15.
//  Copyright (c) 2015 MatthewFecher.com. All rights reserved.
//

import UIKit

//*****************************************************************
// Radio Station
//*****************************************************************

// Class inherits from NSObject so that you may easily add features
// i.e. Saving favorite stations to CoreData, etc

class RadioStation: NSObject {
    
    var musicsid          :String
    var musicsArtistName  :String
    var musicsImage       :String
    var musicsMusicFile   :String
    var musicsType        :String
    var musicsPrice       :String
    var musicsStoreId     :String
    var musicsTitleTrack  :String
    var musicsDuration  :String
    
    
    init(id: String, artist_name: String, image: String, music_file: String, music_type: String, price: String, store_id: String, title_track: String, music_dur: String) {
        self.musicsid         = id
        self.musicsArtistName = artist_name
        self.musicsImage      = image
        self.musicsMusicFile  = music_file
        self.musicsType       = music_type
        self.musicsPrice       = price
        self.musicsStoreId    = store_id
        self.musicsTitleTrack = title_track
        self.musicsDuration = music_dur
    }
    
    //*****************************************************************
    // MARK: - JSON Parsing into object
    //*****************************************************************
        
    class func parseStation(stationJSON: JSON) -> (RadioStation) {
        
        let id           = stationJSON["id"].string ?? ""
        let artist_name  = stationJSON["artist_name"].string ?? ""
        let image        = stationJSON["image"].string ?? ""
        let musicUrl     = stationJSON["music_file"].string ?? ""
        let music_file   = Global.sharedInstance.stringConversionToUTF8(oldStr: musicUrl)
        print(music_file)
        let music_type   = stationJSON["music_type"].string ?? ""
        let price        = stationJSON["price"].string ?? ""
        let store_id     = stationJSON["store_id"].string ?? ""
        let title_track  = stationJSON["title_track"].string ?? ""
        let music_dur  = stationJSON["song_length"].string ?? ""
        
        
        let station = RadioStation(id: id, artist_name: artist_name, image: image, music_file: music_file, music_type: music_type, price: price, store_id: store_id, title_track: title_track, music_dur:music_dur)
        
        return station
    }
    
    class func parseMusicStoreStation(stationJSON: JSON) -> (RadioStation) {
        let id           = stationJSON["id"].string ?? ""
        let artist_name  = stationJSON["artist_name"].string ?? ""
        let image        = stationJSON["image"].string ?? ""
        
        let musicUrl     = stationJSON["music_file"].string ?? ""
        let music_file   = Global.sharedInstance.stringConversionToUTF8(oldStr: musicUrl)
        let music_type   = stationJSON["catogery_songs"].string ?? ""
        let price        = stationJSON["price"].string ?? ""
        let store_id     = stationJSON["store_id"].string ?? ""
        let title_track  = stationJSON["title_track"].string ?? ""
        let music_dur  = stationJSON["song_length"].string ?? ""

        let station = RadioStation(id: id, artist_name: artist_name, image: image, music_file: music_file, music_type: music_type, price: price, store_id: store_id, title_track: title_track, music_dur:music_dur)
        return station
    }
    
    class func parseMyMusic(stationJSON: JSON) -> (RadioStation) {
        let id           = stationJSON["id"].string ?? ""
        let artist_name  = stationJSON["artist_name"].string ?? ""
        let image        = stationJSON["image"].string ?? ""
        let musicUrl     = stationJSON["uploaded_song"].string ?? ""
        let music_file   = Global.sharedInstance.stringConversionToUTF8(oldStr: musicUrl)
        let music_type   = stationJSON["music_type"].string ?? ""
        let price        = stationJSON["price"].string ?? ""
        let store_id     = stationJSON["store_id"].string ?? ""
        let title_track  = stationJSON["post_title"].string ?? ""
        let music_dur  = stationJSON["song_length"].string ?? ""
        
        let station = RadioStation(id: id, artist_name: artist_name, image: image, music_file: music_file, music_type: music_type, price: price, store_id: store_id, title_track: title_track, music_dur:music_dur)
        return station
    }

}
