//
//  AllUserMessageListsView.swift
//  MMC
//
//  Created by Prabhat on 23/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class AllUserMessageListsView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Global.sharedInstance.showLoaderWithMsg((self.view)!)
        NotificationCenter.default.addObserver (
            self,
            selector: #selector(self.removeLoader),
            name: NSNotification.Name(rawValue: "RemoveLoader"),
            object: nil)        // Do any additional setup after loading the view.
    }
    
    func removeLoader() {
        Global.sharedInstance.hideLoader()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
