//
//  MessageViewController.swift
//  MMC
//
//  Created by Prabhat on 22/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import JSQMessagesViewController

// MARK:- Struct For User
class MessageViewController: JSQMessagesViewController, UITextFieldDelegate {
    let currentUser = UserDefaults.standard.string(forKey: "user_id")

    var sendMessage: String? = ""
    var postId: String = ""
    var receiver: String = ""
    var thread_id: String = ""
    var timer: Timer?
    
    var jobPostDetail: [String : AnyObject]?
    
    /* All Messages of User1 and User2 */
    var messages: Array<Any> = [JSQMessage]()
    
    func startTimer() {
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.autoCheckMessagesUpdate), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavBarToTheView()
        self.collectionView.collectionViewLayout.sectionInset = UIEdgeInsets(top: 80, left: 10, bottom: 25, right: 10)
        loadHistory()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
//        RunLoop.currentRunLoop.addTimer(myTimer, forMode: NSRunLoopCommonModes)
    }
    
    
    func autoCheckMessagesUpdate() {
        self.getAllMessages()
    }
    func loadHistory() {
        let oldOffsetReversed: CGFloat? = (collectionView?.contentSize.height)! - (collectionView?.contentOffset.y)!
//        let pageSize: Int = fetchNextPage()
        //reloadData is called inside
        view.layoutIfNeeded()
        let offset: CGFloat? = (collectionView?.contentSize.height)! - oldOffsetReversed!
        collectionView?.contentOffset = CGPoint(x: CGFloat((collectionView?.contentOffset.x)!), y: CGFloat(offset!))
    }
    
    func setNavBarToTheView() {
        let screenSize: CGRect = UIScreen.main.bounds
       // let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: screenSize.size.width, height: 64)) // Offset by 20 pixels vertically to take the status bar into account
        var lbl: UILabel
        lbl = UILabel(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        lbl.backgroundColor =  #colorLiteral(red: 0.2751877904, green: 0.6797052026, blue: 0.9078940153, alpha: 1)
        self.view.addSubview(lbl)
        
        var titleLbl: UILabel
        titleLbl = UILabel(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        titleLbl.text = "Messages"
        titleLbl.textColor = UIColor.white
        titleLbl.textAlignment = NSTextAlignment.center
        self.view.addSubview(titleLbl)
        
        let button = UIButton(frame: CGRect(x: 8, y: 5, width: 30, height: 30))
        let redTapImage = UIImage(named: "downArrow")
        
        button.setImage(redTapImage, for: UIControlState.normal)
        button.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    func pressButton(button: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    /* Helper Method To POST The Data On Server */
    func sendMessageButtonPressed() {
        var urlStr:String = baseUrl
        urlStr.append("/consumer_start_chat.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let sender = UserDefaults.standard.string(forKey: "user_id")
        
        var postString = "post_id=\(postId)&"
        postString.append("sender=\(sender!)&")
        postString.append("current_user_id=\(sender!)&")
        postString.append("receiver=\(receiver)&")
        postString.append("messages=\(sendMessage!)&")
        postString.append("thread_id=\(thread_id)")

        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
        
            
            let success  = json["Success"] as! Int
            
            if success == 1 {
                self.messages  = ((json["message_list"] as? NSArray) as? Array<Any>)!
                print(self.messages)
            }

            DispatchQueue.main.async {
                self.messages = self.getMessages()
                self.collectionView.reloadData()
                self.loadHistory()
            }
        }
        
        task.resume()
    }
    
    func getAllMessages() {
        var urlStr:String = baseUrl
        urlStr.append("/chatlist_thread_ind.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        //let sender = UserDefaults.standard.string(forKey: "user_id")
        
        let postString = "thread_id=\(thread_id)"
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString as! String)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            print(json)
            
            let success  = json["Success"] as AnyObject
            
            if success as! String == "1" {
                self.messages  = ((json["chat_listing_ind"] as? NSArray) as? Array<Any>)!
                print(self.messages)
            }
            
            DispatchQueue.main.async {
                self.messages = self.getMessages()
                self.collectionView.reloadData()
                self.loadHistory()
            }
        }
        
        task.resume()
    }
    
}


// MARK:- Extension For ViewController or Showing The Table View Data In Single Rows
extension MessageViewController {
    
    /* JSQMessagesViewController Delegate Method For Send Button To Send The Message */
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        sendMessage = message?.text
        finishSendingMessage()
        sendMessageButtonPressed()
    }
    
    /* JSQMessagesViewController Delegate Method For Showing The User Name In A Label */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.row]
        let  messageUserName = (message as AnyObject).senderDisplayName
        return NSAttributedString(string: messageUserName!)
    }
    
    /* JSQMessagesViewController Delegate Method For Height For Label (Height Of User Name) */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 20
    }
    
    
    /* JSQMessagesViewController Delegate Method To Show The Image */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
         let message = messages[indexPath.item]
        
        if currentUser == (message as AnyObject).senderId {
            // This Color For Current User
            print("Current User")
        } else {
            // This is For Incoming Message Color
            print("Incoming Message Color")
        }
        
        let avatars: UIImage = UIImage(named: "ChatLogo")!
        
        let ChatImg = JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: avatars, diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
        
        return ChatImg
    }
    
    /* JSQMessagesViewController Delegate Method To Decorate The Background Chat */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubbleFact = JSQMessagesBubbleImageFactory()
        let message = messages[indexPath.row]
        
        let outgoingMessagesColor = #colorLiteral(red: 0.2751877904, green: 0.6797052026, blue: 0.9078940153, alpha: 1)
        let incomingMessagesColor = #colorLiteral(red: 0.9573666453, green: 0.9609026313, blue: 0.9643337131, alpha: 1)
        
        if currentUser == (message as AnyObject).senderId {
            // This Color For Current User
            return bubbleFact?.outgoingMessagesBubbleImage(with: outgoingMessagesColor)
        } else {
            // This is For Incoming Message Color
            return bubbleFact?.incomingMessagesBubbleImage(with: incomingMessagesColor)
        }
    }
    
    /* JSQMessagesViewController Delegate Method To Change The Bubbal Text Color */
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        if (message as AnyObject).senderId == senderId { // 1
            cell.textView?.textColor = UIColor.white // 2
        } else {
            cell.textView?.textColor = #colorLiteral(red: 0.177772522, green: 0.1842073798, blue: 0.187830627, alpha: 1) // 3
        }
        
        return cell
    }
    
    
    /* UICollectionView Delegate Method To Show The Number Of Chat In Section */
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    /* JSQMessagesCollectionView Delegate Method For Message Data For Item At Index */
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row] as! JSQMessageData
    }
}

// MARK:- Extension For ViewController To Get The Dynamic Message
extension MessageViewController {
    /*
     * @return: Void
     * @params: Nil
     * @Desc:   View DidLoad Method To Initial Load Data
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
     
        /*
         * tell JSQMessagesViewController
         * Who is the Current user
         */
        self.senderId = currentUser
        self.senderDisplayName = "Test"
        
        /* Remove The Compose Message Button (Left Side Button Set The Nil To remover)*/
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        /* Calling The getMessages Function To Display The Chat Messages */
        if thread_id != "" {
            self.getAllMessages()
            self.startTimer()

        }
        
        print(" Post ID = ",postId)
        print(" Receiver ID = ",receiver)

    }
}

// MARK:- Extension For ViewController To Get The Dynamic Message
extension MessageViewController {
    /*
     * @return: [JSQMessage]
     * @params: nil
     * @Desc  : This Method Is Used To Rander The Message
     */
    func getMessages() -> [JSQMessage] {
        /* Variable To Store The Messages */
        var messages = [JSQMessage]()

        /* Set Dynamic Message */
        for item in self.messages {
            if let obj = item as? [String: Any] {
                let id: String? = obj["sender"] as? String
                let name: String? = obj["name"] as? String
                let txt: String? = obj["messages"] as? String
                let message1 = JSQMessage(senderId: id, displayName: name, text: txt)
                /* Append The Messages In A Single Variable */
                messages.append(message1!)
            }
        }
        return messages
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.stopTimer()
    }
}
