//
//  AppliedJobCreatorInfoViewController.swift
//  MMC
//
//  Created by Prabhat on 06/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

protocol appliedJobDelegate {
    func autoDismiss();
}

class AppliedJobCreatorInfoViewController: UIViewController, PayPalPaymentDelegate {
    
    @IBOutlet var userImg: UIImageView!
    @IBOutlet var postTitle: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var desc: UITextView!
    @IBOutlet var hireBtn: UIButton!
    
    var delegate: appliedJobDelegate? = nil

    var isJobPaid = false
    var UserInfo : [String: Any] = [:]
    var postId: String = ""
    var imageUrl: String = ""
    var jobPostDetail: [String : AnyObject]?
    var jobPostDiscription: [String : AnyObject]?
    var downloadTask: URLSessionDownloadTask?
    var transactionID: String = ""
    
    var environment:String = PayPalEnvironmentProduction {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    var resultText = ""
    var payPalConfig = PayPalConfiguration()

    override func viewDidLoad() {
        super.viewDidLoad()
        print(imageUrl)
        // Do any additional setup after loading the view.
        print(UserInfo)
        
        imageUrl = UserInfo["user_profile_image"] as! String
        
        //
        userImg.layer.borderWidth = 1
        userImg.layer.masksToBounds = false
        userImg.layer.borderColor = UIColor.white.cgColor
        userImg.layer.cornerRadius = userImg.frame.height/2
        userImg.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkJobStatus()
    }
    func paypalConfig() -> Void {
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "MMC, Inc."
        
        // These URLs are just PayPal merchant policy
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.acceptCreditCards = true
        
        // This is the language with which the PayPal ios sdk will be showed to the user. We use zero as the default language of the device.
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        // If you use the paypal, it will use the address which is registered in paypal. Howeverif you use both, the customer will have the choice to use a diffrent ddress or the one in Paypal. We are going to go for both
        payPalConfig.payPalShippingAddressOption = .none;
    }
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            // print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.resultText = completedPayment.description
            
            // print(completedPayment.description)
            
            let paymentResultDic = completedPayment.confirmation as NSDictionary
            
            let dicResponse: AnyObject? = paymentResultDic.object(forKey: "response") as AnyObject?
            
            print(dicResponse)
            self.transactionID = dicResponse!["id"] as! String
            print(self.transactionID)
            self.hireJob()
        })
    }

    /* Helper Method To POST The Data On Server */
    func checkJobStatus() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        
        var urlStr:String = baseUrl
        
        urlStr.append("consumer_post_details_descriptions.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let creatorId = UserInfo["id"] as! String

        var postString = "post_id=\(postId)&"
            postString.append("id=\(creatorId)")
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            let success = json["Success"] as! String
            
            if success == "1" {
                let stationArray = json["messages"] as! NSArray
                self.jobPostDiscription = (json["description"] as! [String:AnyObject])
                self.jobPostDetail  = stationArray[0] as? [String:AnyObject]
                if let obj = self.jobPostDetail!["payment_status"] as? String {
                    //jobPostDetail!["payment_status"] as! String) != "Not Paid"
                    if obj != "Not Paid" {
                        self.isJobPaid = true
                    }
                }
            }
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.loadJobData()
            }
        }
        
        task.resume()
    }
    
    func loadJobData() -> Void {
        /* self.userImg.text = self.jobPostDetail?["post name"] as? String */
        self.postTitle.text = self.jobPostDetail?["post_title"] as? String
        self.desc.text = self.jobPostDetail?["description"] as? String
        if let price =  self.jobPostDetail?["budget_amount"] as? String {
            self.price.text = "$ " + price
        } else {
            self.price.text = "$ 0.00"
        }
        
        if isJobPaid == true {
            self.hireBtn.setTitle("Hired", for: .normal)
        }
        
        if (imageUrl.contains("http")) {
            
            if let url = URL(string: imageUrl) {
                downloadTask = userImg.loadImageWithURL(url: url) { (image) in
                    // station image loaded
                }
            }
            
        } else if imageUrl != "" {
            userImg.image = UIImage(named: imageUrl)
        } else {
            userImg.image = UIImage(named: "stationImage")
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you want to cancel the job?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelButton = UIAlertAction(title: "NO", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        let okButton = UIAlertAction(title: "YES", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Ok")
            self.cancelJob()
        })
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func hireButton(_ sender: Any) {
       // let obj = self.storyboard
        //obj.jobPostDetail =
        
        var alert = UIAlertController()
        if isJobPaid == true {
            alert = UIAlertController(title: "Already", message: "Hired", preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
            })
            alert.addAction(okButton)

        } else {
            alert = UIAlertController(title: "Pay Now", message: "Please Pay $\(String(describing: self.jobPostDetail?["budget_amount"] as! String)) for Job \(String(describing: self.jobPostDetail?["post_title"] as! String)) ", preferredStyle: UIAlertControllerStyle.alert)
            let dismissButton = UIAlertAction(title: "Cancel", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
            })
            let payNowButton = UIAlertAction(title: "Pay Now", style: .default, handler: { (action) in
                self.payNowButton()
//                self.updateJobStatusForHire()
            })
            
            alert.addAction(dismissButton)
            alert.addAction(payNowButton)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func payNowButton() {
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        // Here are the some item that are being sold for example
        // sku is probably the item id provided by the merchant
        let item = PayPalItem(name:  (String(describing: self.jobPostDetail?["post_title"] as! String)), withQuantity: 1, withPrice: NSDecimalNumber(string: self.jobPostDetail?["budget_amount"] as? String), withCurrency: "USD", withSku: "Hip-0037")
        
        let items = [item]
        
        // The Total Price
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "00")
        let tax = NSDecimalNumber(string: "00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        // The Total Price with tax and shipping
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: (self.jobPostDetail?["post_title"] as? String)!, intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
    }


    /*
     *@param: Title And Message Are The String Parameter
     *@return: Void
     *@des: Alert Message Function
     */
    func alertMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissOkButton = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Ok")
            self.hireJob()
        })
        let dismissButton = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
       })
        alert.addAction(dismissButton)
        alert.addAction(dismissOkButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    /* Helper Method To POST The Data On Server */
    
    func updateJobStatusForHire() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("consumer_payment_screen.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let consumerId = UserDefaults.standard.string(forKey: "user_id")
        let creatorId = UserInfo["id"] as! String
        
        var postString = "creator_id=\(creatorId)&"
        postString.append("post_id=\(postId)&")
        postString.append("consumer_id=\(consumerId!)")
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.hireJob()
            }
        }
        
        task.resume()
    }
    func hireJob() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("consumer_job_hired.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let consumerId = UserDefaults.standard.string(forKey: "user_id")
        let creatorId = UserInfo["id"] as! String

        var postString = "creator_id=\(creatorId)&"
        postString.append("post_id=\(postId)&")
        postString.append("consumer_id=\(consumerId!)")
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.dismiss(animated: true, completion:{
                    self.delegate?.autoDismiss()})
                self.viewDidLoad()
            }
        }
        
        task.resume()
    }
    
    func cancelJob() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("consumer_cancel_post_job.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let consumerId = UserDefaults.standard.string(forKey: "user_id")
        let creatorId = UserInfo["id"] as! String
        
        var postString = "creator_id=\(creatorId)&"
        postString.append("post_id=\(postId)&")
        postString.append("consumer_id=\(consumerId!)")
        
        print(postString)
               
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
            }
        }
        
        task.resume()

        
    }

    
    @IBAction func startChat(_ sender: Any) {
        let creatorId = UserInfo["id"] as! String
        print(jobPostDiscription)
        var thread_id = ""
        if let thread_idd =  jobPostDetail!["thread_id"] {
            thread_id = thread_idd as! String
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MessageViewControllerID") as! MessageViewController
        controller.postId = postId
        controller.receiver = creatorId
        controller.thread_id = thread_id
        print("post ID = ", postId)
        print("Creator ID = ", creatorId)
        present(controller, animated: true, completion: nil)
    }

}
