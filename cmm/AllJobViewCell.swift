//
//  AllJobViewCell.swift
//  MMC
//
//  Created by Prabhat on 05/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class AllJobViewCell: UITableViewCell {

    @IBOutlet var jobTitle: UILabel!
    @IBOutlet var jobDesc: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var jobStatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
