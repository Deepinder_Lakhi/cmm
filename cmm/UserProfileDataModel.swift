//
//  UserProfileDataModel.swift
//  MMC
//
//  Created by Prabhat on 02/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class UserProfileDataModel: NSObject {
    
    var userID            :String
    var userName          :String
    var userPassword      :String
    var userEmail         :String
    var userPhoneNumber   :String
    var userRole          :String
    var gennes            :String
    var customSongsMade   :String
    var priceRange        :String
    var aboutArtist       :String
    var fileImageUpload   :String
    var turnAroundTime    :String
    var userPaypalAccount :String
    var userCountry       :String
    var userState         :String
    var userPinCode       :String
    var userAddress       :String
    var userWalletInfo    :String
    var userAboutMe       :String
    var userProfileImage  :String

    init(id: String, user_name: String, user_password: String, user_email: String, user_phone_number: String, user_role: String, Gennes: String, Custom_songs_made: String, Price_range: String, About_artist: String, turn_around_time: String, file_image_upload: String, Paypal_account: String, Country: String, State: String, Pin_code: String, Address: String, wallet_nfo: String, about_me: String, user_profile_image: String) {
        
        self.userID            = id
        self.userName          = user_name
        self.userPassword      = user_password
        self.userEmail         = user_email
        self.userPhoneNumber   = user_phone_number
        self.userRole          = user_role
        self.gennes            = Gennes
        self.customSongsMade   = Custom_songs_made
        self.priceRange        = Price_range
        self.aboutArtist       = About_artist
        self.fileImageUpload   = file_image_upload
        self.turnAroundTime    = turn_around_time
        self.userPaypalAccount = Paypal_account
        self.userCountry       = Country
        self.userState         = State
        self.userPinCode       = Pin_code
        self.userAddress       = Address
        self.userWalletInfo    = wallet_nfo
        self.userAboutMe       = about_me
        self.userProfileImage  = user_profile_image
    }
    
    class func parseUserData(stationJSON: JSON) -> (UserProfileDataModel) {
        
        let id                  = stationJSON["id"].string ?? ""
        let user_name           = stationJSON["user_name"].string ?? ""
        let user_password       = stationJSON["user_password"].string ?? ""
        let user_email          = stationJSON["user_email"].string ?? ""
        let user_phone_number   = stationJSON["user_phone_number"].string ?? ""
        let user_role           = stationJSON["user_role"].string ?? ""
        let Gennes              = stationJSON["Gennes"].string ?? ""
        let Custom_songs_made   = stationJSON["Custom_songs_made"].string ?? ""
        let Price_range         = stationJSON["Price_range"].string ?? ""
        let About_artist        = stationJSON["About_artist"].string ?? ""
        let turn_around_time    = stationJSON["turn_around_time"].string ?? ""
        let file_image_upload   = stationJSON["file_image_upload"].string ?? ""
        let Paypal_account      = stationJSON["Paypal_account"].string ?? ""
        let Country             = stationJSON["Country"].string ?? ""
        let State               = stationJSON["State"].string ?? ""
        let Pin_code            = stationJSON["Pin_code"].string ?? ""
        let Address             = stationJSON["Address"].string ?? ""
        let wallet_nfo          = stationJSON["wallet_nfo"].string ?? ""
        let about_me            = stationJSON["about_me"].string ?? ""
        let user_profile_image  = stationJSON["user_profile_image"].string ?? ""
        
        let profileData = UserProfileDataModel(id: id, user_name: user_name, user_password: user_password, user_email: user_email, user_phone_number: user_phone_number, user_role: user_role, Gennes: Gennes, Custom_songs_made:Custom_songs_made, Price_range:Price_range, About_artist:About_artist, turn_around_time:turn_around_time, file_image_upload:file_image_upload, Paypal_account:Paypal_account, Country:Country, State:State, Pin_code:Pin_code, Address:Address, wallet_nfo:wallet_nfo, about_me:about_me, user_profile_image:user_profile_image)
        
        return profileData
    }
    
}
