 //
//  LoginViewController.swift
//  MMC
//
//  Created by Maninder on 01/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//


import UIKit


class LoginViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var selectedType:Bool = true
    var user_role = ""

    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.pop(true)
    }
    
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    //@IBAction var loginBtn
    var response = [String:AnyObject]()
    
    
    @IBOutlet weak var fbBtnHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    
    //MARK: API Call
    
    @IBAction func loginButtonPressed() {
        
        if ((txtEmail.text?.isEmpty)! && (txtPassword.text?.isEmpty)!)
        {
            self.alertMessage(title: "Alert", message: "Please make sure all the fields are filled")
        } else {
            loginWithDetails()
        }
    }
    
    func loginWithDetails() {
        
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("login.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        var postString = "users_email=\(txtEmail.text!)&"
        postString.append("users_password=\(txtPassword.text!)")
        
        print(postString)
        
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            var json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            let success = json["Success"] as! String
            
            json["Paypal Address"] = ""
            json["Profile Image"] = ""
            
            /*
             
             
             $_POST['about_artist'];
             $_POST['turn_around_time'];
             $_POST['file_image_upload'];
             $_POST['paypal_account'];
             $_POST['country'];
             $_POST['state'];
             $_POST['pin_code'];
             $_POST['address'];
             $_POST['wallet_nfo'];
             $_POST['about_me'];
             */
            
            print(json)

            
           if success == "1" {
                let defaults = UserDefaults.standard
                defaults.set(json, forKey: "userProfile")

                print(defaults)
  
                
             //   let userType = UserDefaults.standard.string(forKey: "user_type")

             //   self.user_role = userType!
                
               if (json["user_role"] as! String == "creator") {
                    Global.sharedInstance.user_role = "creator"
                } else {
                    Global.sharedInstance.user_role = "consumer"
                }
            
                defaults.set(json["user_id"], forKey: "user_id")
                defaults.set(json["user_role"], forKey: "user_type")
                defaults.set(json["user_name"], forKey: "user_name")
            }
        
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                if success == "1" {
                    self.moveToMainView()
                } else {
                    self.txtPassword.text = nil
                    self.alertMessage(title: "Please Check", message: "Email or password might be wrong")
                }
            }
        }
        task.resume()
    }
    
    
    func convertDataForPicture(_ data:Any) -> (Any) {
        let jsonData = data as? [String:Any]
        let xmlData = jsonData?["data"]
        let json = xmlData as? [String:Any]
        return json!
    }
    
    func moveToMainView() {
        let rootVC = storyboard!.instantiateViewController(withIdentifier: "MusicViewControllerID") as! MusicViewController
        
        UIApplication.shared.keyWindow?.rootViewController = rootVC
    }
    
    
    /*
     *@param: Title And Message Are The String Parameter
     *@return: Void
     *@des: Alert Message Function
     */
    func alertMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Ok")
        })
        alert.addAction(dismissButton)
        self.present(alert, animated: true, completion: nil)
    }
    
}
