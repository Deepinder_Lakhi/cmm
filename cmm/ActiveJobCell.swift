//
//  ActiveJobCell.swift
//  MMC
//
//  Created by Prabhat on 11/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class ActiveJobCell: UITableViewCell {

    @IBOutlet var jobTitle: UILabel!
    @IBOutlet var jobDesc: UILabel!
    @IBOutlet var jobPrice: UILabel!
    @IBOutlet var jobStatus: UILabel!
    @IBOutlet var fullView: UIButton!
    @IBOutlet weak var completeBtn: DLButtonExtender!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
