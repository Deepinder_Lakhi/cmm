//
//  LeaderBoardTableViewCell.swift
//  
//
//  Created by DEEPINDERPAL SINGH on 20/07/17.
//
//

import UIKit

class LeaderBoardTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var rating: HCSStarRatingView!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
