//
//  LeaderBoardViewController.swift
//
//
//  Created by DEEPINDERPAL SINGH on 20/07/17.
//
//

import UIKit

class LeaderBoardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    var selectedMembers: NSMutableArray = []
    
    var creatorID: String = ""
    var allData:Array<Any> = []
    var downloadTask: URLSessionDownloadTask?
    
    
    var delegate: creatorListDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        test()
    }
    
    func test() -> Void {
        // Send HTTP GET Request
        Global.sharedInstance.showLoaderWithMsg(self.view)
        // Define server side script URL
        // Create NSURL Ibject
        let myUrl = NSURL(string: "http://musicmarketcorrection.com/webapp/leadership_board_for_consumer.php");
        
        // Creaste URL Request
        let request = NSMutableURLRequest(url:myUrl! as URL);
        
        // Set request HTTP method to GET. It could be POST as well
        request.httpMethod = "GET"
        
        // Excute HTTP Request
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            // Check for error
            if error != nil
            {
                print("error=\(String(describing: error))")
                Global.sharedInstance.hideLoader()
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            
            // Convert server json response to NSDictionary
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    
                    // Get value by key
                    let success = convertedJsonIntoDict["Success"] as? String
                    
                    if success == "0"{
                        
                        self.finishLoading()
                        
                    }
                    
                    if success! == "1" {
                        self.allData = (convertedJsonIntoDict["top_rated_creator"] as? NSArray as? Array<Any>)!
                        print(self.allData)
                        self.finishLoading()
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
                self.finishLoading()
            }
            
        }
        
        task.resume()
    }
    
    
    func finishLoading() {
        DispatchQueue.main.async {
            Global.sharedInstance.hideLoader()
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderBoardCell", for: indexPath) as! LeaderBoardTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Configure the cell...
        let allData = self.allData[indexPath.row] as! [String: Any]
        
        cell.title.text = (allData["user_name"] as? String)!
        let rating:String = (allData["total_rating"] as? String)!
        //        print(rating)
        
        
        let fir = rating.components(separatedBy: ".")
        var val = "\(fir[0])"
        if fir.count >= 2 {
            val = "\(fir[0]).\(fir[1])"
        }
        
        let ratingVal = Float(val)
        cell.rating.value = CGFloat(CFloat(ratingVal!))
        //        creatorID = (allData["id"] as? String)!
        
        let imageURL = (allData["user_profile_image"] as? String)!
        
        if imageURL.contains("http") {
            
            if let url = URL(string: imageURL) {
                downloadTask = cell.userImgView.loadImageWithURL(url: url) { (image) in
                    // station image loaded
                }
            }
            
        }
        
        return cell
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButton(_ sender: UIButton) {
        let data = selectedMembers
        delegate?.didFinishWith(data: data)
        dismiss(animated: true, completion: nil)
    }
    
}
