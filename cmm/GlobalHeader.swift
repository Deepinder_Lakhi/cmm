//
//  GlobalHeader.swift
//  MMC
//
//  Created by Maninder on 15/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class GlobalHeader: UIView {
    
    static let sharedInstance : GlobalHeader = {
        let instance = GlobalHeader()
        return instance
    }()
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "GlobalHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    //MARK: Initializers
    override init(frame : CGRect) {
        super.init(frame : frame)
        configure()
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    
    func configure() {
        let view = GlobalHeader.instanceFromNib()
        self.addSubview(view)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

}
