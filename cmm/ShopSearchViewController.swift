//
//  ShopSearchViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 02/08/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class ShopSearchViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet var tableView: UITableView!
    var isSearchActive = false
    var searchData:Array<Any> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //UISearchBar delegates
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        print("DidBeginEditing")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("EndEditing")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.text = ""
        //        searchActive = false;
        print("CancelClicked")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false;
        Global.sharedInstance.showLoaderWithMsg(self.view)
        loadSearchData(searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText.characters.count == 0) {
            isSearchActive = false
            self.view.endEditing(true)
            self.tableView.reloadData()
        }
    }
    
    func loadSearchData(_ keyword:String) {
        var urlStr:String = baseUrl
        
        urlStr.append("search_api_all_songs.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let postString = "search_values=\(keyword)"
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            let success  = json["Success"] as AnyObject
            
            if success as! String == "1" {
                self.searchData = []
                self.isSearchActive = true
                self.searchData = json["creator_info"] as! Array
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        task.resume()
    }
}
