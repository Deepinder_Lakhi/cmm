//
//  Global.swift
//  MMC
//
//  Created by Maninder on 07/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit


extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
}

extension UINavigationController {
    func pop(_ animated: Bool) {
        _ = self.popViewController(animated: animated)
    }
    
    func popToRoot(_ animated: Bool) {
        _ = self.popToRootViewController(animated: animated)
    }
    
}


let baseUrl = "http://musicmarketcorrection.com/webapp/"

let userId = UserDefaults.standard.string(forKey: "user_id")

class Global: NSObject {
    
    //MARK: Shared Instance
    
    static let sharedInstance : Global = {
        let instance = Global()
        return instance
    }()
    
    var hud = YBHud()
    
    let defaults = UserDefaults.standard
    
    let apiManager = APIManager()
    
    var isUserCreator:Bool = true
    
    var user_role = ""
    
    let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    func logOutFromApp() {
        
        defaults.removeObject(forKey: "userProfile")
        defaults.removeObject(forKey: "user_id")
    }
    
    func alertMessage(title:String, message:String, mySelf:UIViewController)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Ok")
        })
        alert.addAction(dismissButton)
        mySelf.present(alert, animated: true, completion: nil)
    }

    
    func initUserWithType() {
        let userType = UserDefaults.standard.string(forKey: "user_type")
        user_role = userType!
    }

      /*  if (userType == "creator") {
            print("I'm a creator")
            user_role = userType!
            Global.sharedInstance.isUserCreator = true
        } else {
            print("I'm a consumer")
            Global.sharedInstance.isUserCreator = false
        }
    }
    */
    func moveToMainView() {
        //Removing object from user default
        UserDefaults.standard.removeObject(forKey: "token")
        
        let rootVC:LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerID") as! LoginViewController
        let nvc:UINavigationController = storyboard.instantiateViewController(withIdentifier: "loginRootVC") as! UINavigationController
        nvc.viewControllers = [rootVC]
        UIApplication.shared.keyWindow?.rootViewController = nvc
    }
    
    func setImageFromUrl(_ str:String, completion: @escaping (_ image: UIImage) -> Void) {
        //        SDWebImageManager.shared().downloadImage(with: NSURL(string: str) as URL!, options: .continueInBackground, progress: {
        //            (receivedSize :Int, ExpectedSize :Int) in
        //
        //        }, completed: {
        //            (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
        //
        //            if (error != nil) {
        //                print(error!.localizedDescription as Any)
        //            }
        //            if (image != nil) {
        //                // Finally convert that Data into an image and do what you wish with it.
        //                completion(image!)
        //                // Do something with your image.
        //            }
        //
        //        })
    }
    
    func addLeftAnimation(_ view: UIView) {
        let slideInFromLeftTransition = CATransition()
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.5
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    
    func addRightAnimation(_ view: UIView) {
        let slideInFromLeftTransition = CATransition()
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromRight
        slideInFromLeftTransition.duration = 0.5
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    
    func getUserID()  {
        
    }
    
    func showLoaderWithMsg(_ view:UIView) {
        hud = YBHud.init(hudType: DGActivityIndicatorAnimationType.lineScalePulseOut)
        hud.dimAmount = 0.7
        hud.show(in: view, animated: true)
    }
    
    
    func hideLoader() {
        hud.dismiss(animated: true)
    }
    
    
    //    func loginAlert() {
    //        let alertController = UIAlertController(title: "Please make sure all the fields are filled", message:"", preferredStyle: UIAlertControllerStyle.alert)
    //        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
    //
    //        self.present(alertController, animated: true, completion: nil)
    //    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        print(body)
        
        return body
    }
    
    func stringConversionToUTF8(oldStr: String) -> String {
        
        let esc_str = oldStr.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        /*var newString = String()
        do {
            newString = try String(contentsOfFile: oldStr, encoding: .utf8)
        } catch {
            newString = oldStr
        }
 */
        print("esc_str: \(String(describing: esc_str))")
        return esc_str!
    }
}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
   
}

