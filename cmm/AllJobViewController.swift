//
//  AllJobViewController.swift
//  MMC
//
//  Created by Prabhat on 29/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class AllJobViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var tableview: UITableView!

    var allPosts:Array<Any> = []
    
    fileprivate let image = UIImage(named: "star-large")!.withRenderingMode(.alwaysTemplate)
    
    fileprivate let topMessage = "Favorites"
    
    fileprivate let bottomMessage = "You don't have any favorites yet. All your favorites will show up here."
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.hideKeyboardWhenTappedAround()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchAllJob()
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.dismissKeyboard()
    }
       
    func setupEmptyBackgroundView() {
        let emptyBackgroundView = EmptyBackgroundView(image: image, top: topMessage, bottom: bottomMessage)
        print(emptyBackgroundView)
        tableview.backgroundView = emptyBackgroundView
    }
    
    /* Helper method to fetch all jobs from for user */
    func fetchAllJob() {
        var urlStr:String = baseUrl
        urlStr.append("consume_all_jobs.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")
        let postString = "consumer_id=\(userID!)"
        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                self.removeLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                self.removeLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            print(jsonData!)
        
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            let success  = json["Success"] as AnyObject

            if success.boolValue == true {
                self.allPosts = (json["consumer_job_lists"] as! NSArray) as! Array<Any>
                print(self.allPosts)
            }
                                   
            DispatchQueue.main.async {
                self.removeLoader()
                self.tableView.reloadData()
            }
        }
        
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "All")
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allPosts.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Jobs Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allPosts.count
    }
    
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            removeLoader()
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllJobViewID", for: indexPath) as! AllJobViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let title = allPosts[indexPath.row] as! [String: Any]
        cell.jobTitle.text = title["title"] as? String
        
        let jobsDesc = allPosts[indexPath.row] as! [String: Any]
        cell.jobDesc.text = jobsDesc["song_content"] as? String
        
        let jobsPrice = allPosts[indexPath.row] as! [String: Any]
        let price = jobsPrice["budget"] as? String
        cell.price.text = "$ " + price!
        
        let JobsStatus = allPosts[indexPath.row] as! [String: Any]
        let status = JobsStatus["status"] as? String
        print(status)
        cell.jobStatus.text? = (status?.uppercaseFirst)!
        
        return cell
    }

    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ListOfPostAppliedJobByCreatorViewID") as! ListOfPostAppliedJobByCreatorView
        let postID = allPosts[indexPath.row] as! [String: Any]
        let postId = postID["post_id"]
          controller.postId = postId as! String
        present(controller, animated: true, completion: nil)
        /*
        let goToNextView = self.storyboard?.instantiateViewController(withIdentifier: "ListOfPostAppliedJobByCreatorViewID") as? ListOfPostAppliedJobByCreatorView
        self.navigationController?.pushViewController(goToNextView!, animated: true)*/
    }
    
    func removeLoader() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveLoader"), object: nil, userInfo: nil)
    }
    
    
}
