//
//  ProfileEditViewController.swift
//  MMC
//
//  Created by Prabhat on 12/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class ProfileEditViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var userName: UITextField!
    @IBOutlet var userAddress: UITextField!
    @IBOutlet var userCountrey: UITextField!
    @IBOutlet var userState: UITextField!
    @IBOutlet var pinCode: UITextField!
    @IBOutlet var payPal: UITextField!
    @IBOutlet var aboutDesc: UITextView!
    @IBOutlet var scrollView: UIScrollView!
    
    var getAlldata:Array<Any> = []
    var amt: Int = 0

    var name: String? = ""
    var address: String? = ""
    var contery: String? = ""
    var state: String? = ""
    var pinCodeInfo: String? = ""
    var payPalID: String? = ""
    var aboutDes: String? = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        getAllDataToEdit()
        self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: self.scrollView.contentSize.height)

    }
    
    /* Helper Method To POST The Data On Server */
    func getAllDataToEdit() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("users_profile_fetch.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        
        let userID = UserDefaults.standard.string(forKey: "user_id")
        
        let postString = "id=\(userID!)"
        
        print(postString)
        
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            let stationArray = json["edit_profile"] as! NSArray
            
            let allData  = stationArray[0] as! [String:AnyObject]
            
            print(allData["user_profile_image"] as? String? as Any)
            
            self.name         = allData["user_name"] as? String!
            self.address      = allData["Address"] as? String!
            self.contery      = allData["Country"] as? String!
            self.state        = allData["State"] as? String!
            self.pinCodeInfo  = allData["Pin_code"] as? String!
            self.payPalID     = allData["Paypal_account"] as? String!
            self.aboutDes     = allData["about_me"] as? String!
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.loadProfileData()
            }
        }
        
        task.resume()
    }
    
    
    func loadProfileData() -> Void {
        self.userName.text = name
        self.userAddress.text = address
        self.userCountrey.text = contery
        self.userState.text = state
        self.pinCode.text = pinCodeInfo
        self.payPal.text = payPalID
        self.aboutDesc.text = aboutDes
    }
    


    @IBAction func backArrow(_ sender: UIButton) {
       dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateProfile(_ sender: Any) {
        if userName.text == "" || userAddress.text == "" || userCountrey.text == "" || userState.text == "" || pinCode.text == "" || payPal.text == "" || aboutDesc.text == "" {
            errorMessage(title: "Error", message: "Please Fill The All fields")
        } else {
            updataAllDataByButton()
        }
    }
    
    /* Helper Method To POST The Data On Server */
    func updataAllDataByButton() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("/users_profile_edit.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        
        let userID = UserDefaults.standard.string(forKey: "user_id")
        
        var postString = "id=\(userID!)&"
        postString.append("user_name=\(userName.text!)&")
        postString.append("Country=\(userCountrey.text!)&")
        postString.append("State=\(userState.text!)&")
        postString.append("Pin_code=\(pinCode.text!)&")
        postString.append("Address=\(userAddress.text!)&")
        postString.append("wallet_nfo=0&")
        postString.append("Paypal_account=\(payPal.text!)&")
        postString.append("about_me=\(aboutDesc.text!)")
        
        print(postString)
        
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.userName.text = ""
                self.userAddress.text = ""
                self.userCountrey.text = ""
                self.userState.text = ""
                self.pinCode.text = ""
                self.payPal.text = ""
                self.aboutDesc.text = ""
                self.backNav()
            }
        }
        
        task.resume()
    }
    
    func backNav() -> Void {
        dismiss(animated: true, completion: nil)
    }
    
    /*
     *@param: Title And Message Are The String Parameter
     *@return: Void
     *@des: Alert Message Function
     */
    func errorMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        alert.addAction(dismissButton)
        self.present(alert, animated: true, completion: nil)
    }


}
