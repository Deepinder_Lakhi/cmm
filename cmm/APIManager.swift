//
//  APIManager.swift
//  mermaids
//
//  Created by DAVID on 16/01/17.
//  Copyright © 2017 Jingged. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    
    static let USER_COUNT = "30"
    /**
     ** Check if access token in valid If status is TRUE then it's valid and app RUN smoothly. If not then it logged user out automatically.
     **/
    
    func checkTokenValidation(_ completion: @escaping (_ status: Bool) -> Void) {
        
        //Use Alamofire Next time
        
        var request = URLRequest(url: URL(string:baseUrl)!)
        request.httpMethod = "POST"
        
        let tokenInfo = UserDefaults.standard.string(forKey: "token")
        
        var postString = "action=check_token&"
        
        postString.append("vtoken=\(tokenInfo!)&")
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            self.catchError(<#([Any]) -> Void#>)
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            let responseString = String(data: data, encoding: .utf8)
            print(responseString!)
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            print(json)
            
            let success = json["success"] as! NSNumber
            
            if success == 1  {
                completion(true)
            } else {
                completion(false)
            }
        }
        
        task.resume()
    }
    /**
     ** Getting users to swipe
     **/
    func getUsers(_ completion: @escaping (_ details:[Any]) -> Void) {
        var request = URLRequest(url: URL(string:"http://api.mermaidsdating.com/index.php")!)
        request.httpMethod = "POST"
        let tokenInfo = UserDefaults.standard.string(forKey: "token")
        
        var postString = "action=Swipuser&"
        postString.append("limit=\(APIManager.USER_COUNT)&")
        postString.append("token=\(tokenInfo!)&")
        postString.append("current_page=1")
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            //
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            var details:[Any] = []
            
            details = json["details"] as! [Any]
            
            completion(details)
        }
        task.resume()
    }
    
    class func getListOfMusics(_ completion: @escaping (_ details:NSArray) -> Void) {
        
        var urlStr:String = baseUrl
        
        urlStr.append("shop_page.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        

        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! NSDictionary
            
            completion(json.object(forKey: "shop_history") as! NSArray)
            
        }
        
        task.resume()
    }


    
    class func getListOfMusicsStation( type:String ,_ completion: @escaping (_ details:Data) -> Void) {
        
        var urlStr:String = baseUrl
        
        urlStr.append("shop_page.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let userID = UserDefaults.standard.string(forKey: "user_id")

        let postString = "catogery_songs_lists=\(type)&user_id=\(String(describing: userID!))"
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            completion((jsonData! as Data))
            
        }
        
        task.resume()
    }
    
    
    class func getListOfJobData(_ completion: @escaping (_ details:[String:Any]) -> Void) {
        
        var urlStr:String = baseUrl
        let userID = UserDefaults.standard.string(forKey: "user_id")!

        urlStr.append("consumer_jobpost_lists.php")
        urlStr.append("?UID=\(userID)")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            completion((json))
        }
        task.resume()
    }
    
    
    
    class func getListOfAllCreators(_ completion: @escaping (_ details:[String:Any]) -> Void) {
        
        var urlStr:String = baseUrl
        
        urlStr.append("send_inviataion_listofall_creators.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString as Any)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            completion((json))
        }
        
        task.resume()
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func catchError(_ completion: @escaping (_ details:[Any]) -> Void)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}
