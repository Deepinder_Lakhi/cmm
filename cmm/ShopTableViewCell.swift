//
//  ShopTableViewCell.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 23/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class ShopTableViewCell: UITableViewCell {

    @IBOutlet weak var songTitleHead: UILabel!
    @IBOutlet weak var artistText: UILabel!
    @IBOutlet weak var durationText: UIButton!
    @IBOutlet weak var genreText: UIButton!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet var buyButton: DLButtonExtender!
    
    @IBOutlet weak var artistInfo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
