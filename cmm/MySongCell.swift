//
//  MySongCell.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 24/07/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class MySongCell: UITableViewCell {

    
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var SongDur: UIButton!
    @IBOutlet var songType: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var userImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
