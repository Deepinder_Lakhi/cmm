//
//  ApplyNowViewController.swift
//  MMC
//
//  Created by Prabhat on 01/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class ApplyNowViewController: UIViewController, UITextFieldDelegate {
    
    var applyId: String = ""

    @IBOutlet weak var budget: UITextField!
    @IBOutlet weak var coverLetter: UITextView!
    @IBOutlet weak var estimatatedHours: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)

    }

    @IBAction func submitButton(_ sender: Any) {
        if budget.text == "" || coverLetter.text == "" || estimatatedHours.text == "" {
            errorMessage(title: "Error", message: "Please Fill The All fields")
        } else {
            postJob()
        }
    }
    
    /* Helper Method To POST The Data On Server */
    func postJob() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("consumer_post_job.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let userID = UserDefaults.standard.string(forKey: "user_id")
        
        var postString = "id=\(userID!)&"
        postString.append("post_id=\(applyId)&")
        postString.append("decsription=\(coverLetter.text!)&")
        postString.append("budget=\(budget.text!)&")
        postString.append("estimated_hours=\(estimatatedHours.text!)")

        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.errorMessage(title: "Success", message: "Request Posted Successfuly!")

                self.clearTextFields()

            }
        }
        
        task.resume()
    }
    
    func clearTextFields() -> Void {
        budget.text = nil
        estimatatedHours.text = nil
        coverLetter.text = nil
    }
    
    /*
     *@param: Title And Message Are The String Parameter
     *@return: Void
     *@des: Alert Message Function
     */
    func errorMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.dissmis()
        })
        alert.addAction(dismissButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func dissmis() -> Void {
        dismiss(animated: true, completion: nil)
    }

}
