//
//  AllMessageCell.swift
//  MMC
//
//  Created by Prabhat on 23/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class AllMessageCell: UITableViewCell {

    @IBOutlet var userImg: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var messageDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
