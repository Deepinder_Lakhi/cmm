//
//  ActiveJobCreatorViewController.swift
//  MMC
//
//  Created by Prabhat on 30/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ActiveJobCreatorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!
    var allPosts:Array<Any> = []
    
    var postId: String = ""
    var consumerId: String = ""
    var thread_id: String = ""
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var jobDataModel = [JobDataModel]()
    
    // don't forget to hook this up from the storyboard
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        // Custom Table View Cell Identifier
        tableView.register(UINib(nibName:"JobTableViewCell", bundle: nil), forCellReuseIdentifier: "JobTableViewID")
        

                /*
         Global.sharedInstance.showLoaderWithMsg(self.view)
         APIManager.getListOfJobData({
         (details: Data) in
         let json = JSON(data: details as Data)
         print(json)
         if let stationArray = json["job_lists"].array {
         
         for stationJSON in stationArray {
         let station = JobDataModel.parseStation(stationJSON: stationJSON)
         self.jobDataModel.append(station)
         }
         // stations array populated, update table on main queue
         self.finishLoading()
         }
         })
         */
        
    }
    
    /*
     func finishLoading() {
     DispatchQueue.main.async {
     Global.sharedInstance.hideLoader()
     self.tableView.delegate = self
     self.tableView.dataSource = self
     self.tableView.reloadData()
     }
     }
     */
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismissKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
       
    }
    override func viewWillLayoutSubviews() {
         postJob()
    }
    
    /* Helper Method To POST The Data On Server */
    func postJob() {
        var urlStr:String = baseUrl
        let userID = UserDefaults.standard.string(forKey: "user_id")

        urlStr.append("creator_applied_job_list.php")
        urlStr.append("?creator_id=\(String(describing: userID!))")
//        http://musicmarketcorrection.com/webapp/creator_applied_job_list.php?creator_id=1
        print(urlStr)
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "GET"
//        let postString = "creator_id=\(userID!)"
//
//        print(postString)
//        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                self.removeLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                self.removeLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            self.allPosts = (json["creator info"] as! NSArray) as! Array<Any>
            print(self.allPosts)
            
            DispatchQueue.main.async {
                self.removeLoader()
                self.tableView.reloadData()
            }
        }
        
        task.resume()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "ACTIVE")
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allPosts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        removeLoader()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobTableViewID", for: indexPath) as! JobTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Configure the cell...
        let jobData = allPosts[indexPath.row] as! [String: Any]
        print(jobData)
        cell.songTitle.text = jobData["post_title"] as? String
        cell.songContent.text = jobData["description"] as? String
        
        let status = jobData["status"] as? String
        
        cell.genre.text = "Status: " + status!.uppercaseFirst
        
        let price = jobData["budget_amount"] as? String
        
        let threadID = jobData["thread_id"] as? String
        
        let length = jobData["estimaed_hours"] as? String
        
        if price == nil {
            cell.budgetPrice.text = "$ 00.00"
        } else {
            cell.budgetPrice.text = "$ " + price!
        }
        
        // let songLength = jobData["songLength"] as? String
        let dateString = jobData["time"] as? String
        
//        let dateFromString =
        //stringFromDate
        cell.songLength.text = "Estimated Hours: \(length!)"
        cell.timeAgo.text = dateString
        cell.jobStatus.isHidden = true
        
        if threadID != "0" {
            cell.moreView.isHidden = false
            cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action: #selector(self.showActionSheet(sender:)), for: .touchUpInside)
        }
        cell.fullView.tag = indexPath.row
        cell.fullView.addTarget(self, action: #selector(self.showDetails(sender:)), for: .touchUpInside)
        
        cell.jobStatus.tag = indexPath.row
        
//        cell.jobStatus.addTarget(self, action: #selector(self.selected(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func showDetails(sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "JobDetailCreatorViewController") as! JobDetailCreatorViewController
        let jobData = allPosts[sender.tag] as! [String:Any]
        controller.userInfo = jobData
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func showActionSheet(sender: AnyObject) {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let jobData = allPosts[sender.tag] as! [String:Any]

        let status = jobData["status"] as! String

        let startChatAction = UIAlertAction(title: "Start Chat", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.selected(sender: sender)
        })
        optionMenu.addAction(startChatAction)

        if status == "hire" {
            let completeJobAction = UIAlertAction(title: "Complete Job", style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    self.completed(sender: sender)
            })
            optionMenu.addAction(completeJobAction)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    
    func selected(sender: AnyObject) {
        
        let selectedData = allPosts[sender.tag] as AnyObject
        postId = selectedData["post_id"] as! String
        consumerId = selectedData["consumer_id"] as! String
        thread_id = selectedData["thread_id"] as! String
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MessageViewControllerID") as! MessageViewController
        controller.postId = postId
        controller.receiver = consumerId
        controller.thread_id = thread_id
        print("Post-ID = ",postId, "Consumer-ID = ", consumerId)
        
        present(controller, animated: true, completion: nil)
    }
    
    func completed(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cellData = allPosts[(sender as AnyObject).tag] as! [String: Any]

        let controller = storyboard.instantiateViewController(withIdentifier: "JobCompletionViewControllerID") as! JobCompletionViewController
        controller.post_id = cellData["post_id"] as! String
        present(controller, animated: true, completion: nil)
    }

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allPosts.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Active Job"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func removeLoader() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveLoader"), object: nil, userInfo: nil)
    }
}
