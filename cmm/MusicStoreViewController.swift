//
//  MusicStoreViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 25/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import Alamofire
import MediaPlayer
import CoreAudio
import MediaPlayer


extension CGRect {
    init(_ x:CGFloat, _ y:CGFloat, _ w:CGFloat, _ h:CGFloat) {
        self.init(x:x, y:y, width:w, height:h)
    }
}
extension CGSize {
    init(_ width:CGFloat, _ height:CGFloat) {
        self.init(width:width, height:height)
    }
}
extension CGPoint {
    init(_ x:CGFloat, _ y:CGFloat) {
        self.init(x:x, y:y)
    }
}
extension CGVector {
    init (_ dx:CGFloat, _ dy:CGFloat) {
        self.init(dx:dx, dy:dy)
    }
}




extension MusicStoreViewController : MPMediaPickerControllerDelegate {
    // must implement these, as there is no automatic dismissal
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        print("Did Finish with data: \(mediaItemCollection)")
        self.dismiss(animated:true)
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        print("cancel")
        self.dismiss(animated:true)
    }
    
}

extension MusicStoreViewController : UIBarPositioningDelegate {
    func positionForBar(forBar bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}


class MusicStoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var uploadSection: UIView!
    @IBOutlet weak var hightConstraintOfUploadSection: NSLayoutConstraint!
    @IBOutlet weak var uploadViewHeightVConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var player: AVPlayer!
    var observer: Any!
    
    var dataRecieved = NSArray()
    var isUploadNeeded: Bool = false
    var currentTag:Int?
    var stations = [RadioStation]()

    @IBOutlet weak var titleLbl: UILabel!
    var allPosts:Array<Any> = []

    var generalInfo = NSDictionary()
    
    var dataTableView = NSArray()
    
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "musicCell"
    
    @IBOutlet weak var artistNameLbl: UILabel?
    @IBOutlet weak var aboutTxt: UITextView?
    @IBOutlet weak var genreTxtLbl: UILabel?
    @IBOutlet weak var prizeTxtLbl: UILabel?
    @IBOutlet weak var turnroundTxtLbl: UILabel?
    
    var img: String?         = ""
    var userName: String?    = ""
    var abouAartist: String? = ""
    var genre: String?       = ""
    var priceRange: String?  = ""
    var turnAround: String?  = ""
    var titleText: String?  = ""
    var userID: String? = ""
    var downloadTask: URLSessionDownloadTask?
    let stopWatch = StopWatch()

    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.initializeUI()
        var size = self.view.frame.size
        size.height = 1000
        
        imageView.layer.masksToBounds = false
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true
        self.scrollView.contentSize = size
        if !isUploadNeeded {
            hightConstraintOfUploadSection.constant = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* Calling The Get User Data From HTTP Api */
        getUserProfileData()
        titleLbl.text = titleText
    }
    
    override func viewWillLayoutSubviews() {
    }
    
    /* Helper Method To POST The Data On Server */
    
    func getUserProfileData() -> Void {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("/login_user_data.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let postString = "id=\(userID!)"
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            print(json)
            
            let stationArray = json["users_details"] as! NSArray
            
            print(stationArray)
            
            let allData  = stationArray[0] as! [String:AnyObject]
            
            self.img         = allData["user_profile_image"] as? String
            self.userName    = allData["user_name"] as? String
            self.abouAartist = allData["about_me"] as? String
            if self.abouAartist == nil {
               self.abouAartist = "Dummy Text"
            }
            self.genre       = allData["Gennes"] as? String
            self.priceRange  = allData["Price_range"] as? String
            self.turnAround  = allData["turn_around_time"] as? String
            
            let fileUrl = Foundation.URL(string: self.img!)
            if fileUrl != nil {
                print(fileUrl!)
                self.downloadImage(url: fileUrl!)
            }
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.fetchStorePost()
                /* Claling The Show Data method */
                self.showDataInUIObjects()
            }
        }
        task.resume()
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { () -> Void in
                self.imageView.image = UIImage(data: data)
                Global.sharedInstance.hideLoader()
            }
        }
    }
    
    /*
     * To Show The Backend Data in UI Objects
     */
    func showDataInUIObjects() -> Void {
         if (self.img != nil) {
           // self.imageView.image = UIImage(named: self.img!)
        }
        self.artistNameLbl?.text = self.userName?.uppercaseFirst
        self.aboutTxt?.text = abouAartist?.uppercaseFirst
        self.genreTxtLbl?.text = self.genre?.uppercaseFirst
        self.prizeTxtLbl?.text = self.priceRange
        self.turnroundTxtLbl?.text = self.turnAround
        
    }

    
    
    @IBAction func backBtn() {
        self.dismiss(animated: true, completion: nil)
    }
   
    func resetPlayer() {
        //Here we are reserting the player for the next use
        player.pause()
        self.currentTag = self.stations.count + 1
        self.tableView.reloadData()
    }
    
    func resumePlayer() {
        //Here we are reserting the player for the next use
        player.play()
        self.currentTag = self.stations.count + 1
        self.tableView.reloadData()
    }
    
    
    @IBAction func play(_ sender: UIButton) {
        let tag = sender.tag
        if tag == currentTag {
            sender.isSelected = !sender.isSelected
            if(sender.imageView?.image == UIImage(named: "icon-play")) {
                sender.setImage(UIImage(named: "icon-pause"), for: UIControlState.normal)
                player.play()
            } else {
                sender.setImage(UIImage(named: "icon-play"), for: UIControlState.normal)
                player.pause()
            }
        } else {
            currentTag = tag
            tableView.reloadData()
        }
    }
    
    func playSong(url:URL, isBoundryNeeded:Bool) {
        player = AVPlayer(url: url)
        player.play()
        if isBoundryNeeded == true {
            let boundary: TimeInterval = 10
            let times = [NSValue(time: CMTimeMake(Int64(boundary), 1))]
            observer = player.addBoundaryTimeObserver(forTimes: times, queue: nil) {
                [weak self] time in
                print("10s reached")
                if let observer = self?.observer {
                    self?.player.removeTimeObserver(observer)
                }
                self?.resetPlayer()
            }
        }
    }

    

    @IBAction func doGo (_ sender: Any!) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AllMusicsFilesViewControllerID") as! AllMusicsFilesViewController        
        present(controller, animated: true, completion: nil)
    }
    
    func fetchStorePost() {
       // Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("creator_store_page.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        print("UserID before fetching the store data : \(userID)")

        let postString = "id=\(userID!)"
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            print(jsonData!)
            
            let fullData = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            
            let json = JSON(data: data as Data)
            print(json)
            self.stations = [RadioStation]()
            let success  = fullData["Success"] as AnyObject
            print(success)
            
                if success.boolValue == true {
                self.allPosts = json["music_history"].array!
                print(self.allPosts)
                for stationJSON in self.allPosts {
                    print("Data to copy: \(stationJSON)")
                    let station = RadioStation.parseMusicStoreStation(stationJSON: stationJSON as! JSON) 
                    self.stations.append(station)
                }
                print(self.stations)
            
            }
//            let stationArray = json["users_details"] as! NSArray
//            print(stationArray)
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                self.tableView.reloadData()
            }
        }
        
        task.resume()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allPosts.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Uploads Yet!"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stations.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicTableViewCellID", for: indexPath) as! MusicTableViewCell
        
        let station = stations[indexPath.row]
        cell.songTitle.text = station.musicsTitleTrack
        cell.songType.titleLabel?.text = " Music Type:  \(station.musicsType)"
        cell.playButton.tag = indexPath.row
        
        let imageURL = station.musicsImage as String
        
        if imageURL.contains("http") {
            if let url = URL(string: station.musicsImage) {
                downloadTask = cell.userImg.loadImageWithURL(url: url) { (image) in
                    // station image loaded
                    cell.userImg.image = image
                }
            }
        } else if imageURL != "" {
            cell.userImg.image = UIImage(named: imageURL as String)
        } else {
            cell.userImg.image = UIImage(named: "stationImage")
        }
        
        if indexPath.row == currentTag {
            cell.playButton.setImage(UIImage(named: "icon-pause"), for: UIControlState.normal)
            
            let musicURL = station.musicsMusicFile as String
            
            self.playSong(url: URL(string:musicURL)!, isBoundryNeeded: false)
            
        } else {
            cell.playButton.setImage(UIImage(named: "icon-play"), for: UIControlState.normal)
        }
        cell.buyButton.isHidden = true
        
        if !isUploadNeeded {
            cell.buyButton.addTarget(self, action: #selector(self.selectedRadioStation), for: .touchUpInside)
            cell.buyButton.tag = indexPath.row
            cell.buyButton.isHidden = false
        }
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    /* Buy Button */
    func selectedRadioStation(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SongDetailViewControllerID") as! SongDetailViewController
        let station = stations[sender.tag]
        controller.item = station as RadioStation
        //  controller.creatorID = stations
        present(controller, animated: true, completion: nil)
    }

    
    
}
