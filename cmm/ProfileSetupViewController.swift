//
//  ProfileSetupViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 16/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import Alamofire


class ProfileSetupViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var containerView: DLViewExtender!
    @IBOutlet var imageView: UIImageView!
    
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var paypalTxtFld: UITextField!
    @IBOutlet weak var countryTxtFld: UITextField!
    @IBOutlet weak var stateTxtFld: UITextField!
    @IBOutlet weak var pinTxtFld: UITextField!
    @IBOutlet weak var addressFld: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize = self.containerView.frame.size
    }

    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    @IBAction func btnClicked() {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        let selectedImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        // Set PhotoImageView to display the selected image
        imageView.image = selectedImage
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createProfileBtnPressed() {
        print("\(paypalTxtFld.text!)")
        print("\(countryTxtFld.text!)")
        print("\(stateTxtFld.text!)")
        print("\(pinTxtFld.text!)")
        print("\(addressFld.text!)")
        let allObject = [paypalTxtFld.text!, countryTxtFld.text!, stateTxtFld.text!, pinTxtFld.text!, addressFld.text!]
        var success:Bool = false
        for object in allObject {
            if (object.isEmpty) {
                success = false
                break
            } else {
                success = true
            }
        }
        
        if success
        {
            Global.sharedInstance.showLoaderWithMsg(self.view)
            createProfile(imageView.image!)
        }
        else {
            print("Not yet")
            showAlert()
        }
    }
    
    func showAlert() {
        let alertController = UIAlertController(title: "Please make sure all the fields are filled", message:"", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }

    
    func createProfile(_ image: UIImage) {
        
        let imageData = UIImageJPEGRepresentation(image, 1)
        
        let userID = UserDefaults.standard.string(forKey: "user_id")!
        
        let parameters = [
            "paypal_info": "\(self.paypalTxtFld.text!)",
            "Country"    : "\(self.countryTxtFld.text!)",
            "State"      : "\(self.stateTxtFld.text!)",
            "Pin_code"   : "\(self.pinTxtFld.text!)",
            "Address"    : "\(self.addressFld.text!)",
            "id"         : "\(userID)",
        ]
        
        print(parameters)
        
        var urlStr = String()
        
        if Global.sharedInstance.user_role == "creator" {
            urlStr = "creator_user_extra_profile.php"
        } else {
            urlStr = "consumer_extra_profile.php"
        }
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "fileset",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:"\(baseUrl)"+urlStr)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    
                    if progress.fractionCompleted == 1.0 {
                        DispatchQueue.main.async {
                            Global.sharedInstance.hideLoader()
                            self.moveToMainView()
                        }
                    }
                })
                
                upload.responseJSON { response in
                    //print(response.result.value!)
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    func moveToMainView() {
        var VC = UIViewController()
        if Global.sharedInstance.user_role == "creator" {
            VC = storyboard!.instantiateViewController(withIdentifier: "CreateStoreViewControllerID") as! CreateStoreViewController
            self.navigationController?.pushViewController(VC, animated: true)

        } else {
            let rootVC = storyboard!.instantiateViewController(withIdentifier: "MusicViewControllerID") as! MusicViewController
            
            UIApplication.shared.keyWindow?.rootViewController = rootVC
        }
        
    }



}
