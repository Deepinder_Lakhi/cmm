//
//  AppDelegate.swift
//  MMC
//
//  Created by Maninder on 01/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import IQKeyboardManager
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "Acu29on1SIhCHO5Asd7Hsm88-XK1M7OuFD7KXGGJInErWXAlwE26lscqEnf6V_l23iaOHBJQPrDNlCr6", PayPalEnvironmentSandbox: "AQxShYEgLDviYD6naN0fCfH3R8RewyvDPn4Wb6sQoF-iRbza20H_SvEJt8RvZ-D4WMS4L2obmsW6S7cr"])
        
        //        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentSandbox: "AerUYXMkWFJ635kG0iZ_kbKqrlYSQY8lR-AUs8XsZsbsgR5YiPckm1RuCiSH-KyObkYbu5tCUG8Awc-9"])
        
        //Override point for customization after application launch.
        IQKeyboardManager.shared().isEnabled = true
        
        /* */
        self.window =  UIWindow(frame: UIScreen.main.bounds)
        if  UserDefaults.standard.string(forKey: "user_type") != nil {
            let userID = UserDefaults.standard.string(forKey: "user_id")
            if (userID != nil) {
                self.launchWindowShoppingScreen()
            } else {
                self.launchTutorialScreen()
            }
        } else {
            self.launchTutorialScreen()
        }
        
        self.window?.makeKeyAndVisible()
        
        
        return true
    }
    
    
    func launchTutorialScreen() {
        //        let rootVC = self.storyboard?.instantiateViewController(withIdentifier: "TutorialViewControllerID") as! TutorialViewController
        //        let nvc:UINavigationController = self.storyboard?.instantiateViewController(withIdentifier: "IntroNav") as! UINavigationController
        //        nvc.viewControllers = [rootVC]
        //        UIApplication.shared.keyWindow?.rootViewController = nvc
        
        let rootVC = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerID") as! TutorialViewController
        let nvc:UINavigationController = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "main") as! UINavigationController
        nvc.viewControllers = [rootVC]
        self.window?.rootViewController = nvc
        
    }
    
    func launchLoginScreen() {
        let rootVC = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "LaunchViewControllerID") as! LaunchViewController
        let nvc:UINavigationController = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "main") as! UINavigationController
        nvc.viewControllers = [rootVC]
        self.window?.rootViewController = nvc
    }
    
    func launchWindowShoppingScreen() {
        Global.sharedInstance.initUserWithType()
        let rootVC = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "MusicViewControllerID") as! MusicViewController
        self.window?.rootViewController = rootVC
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

