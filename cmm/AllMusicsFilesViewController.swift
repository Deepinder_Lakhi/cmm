//
//  AllMusicsFilesViewController.swift
//  MMC
//
//  Created by Prabhat on 23/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

protocol allMusicDelegate {
    func didFinishWith(songName: String, songUrl: URL)
}


class AllMusicsFilesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView: UITableView!
    
    
    @IBAction func showTutorail(_ sender: Any) {
        let nextScene =  self.storyboard?.instantiateViewController(withIdentifier: "WebShellViewControllerID") as! WebShellViewController
        nextScene.myTitle = "Upload Song"
        // Pass the selected object to the new view controller.
        nextScene.urlString = "http://musicmarketcorrection.com/webapp/upload-songs.php"
        self.present(nextScene, animated: true, completion: nil)
    }

    @IBAction func reloadData(_ sender: Any) {
        allmMsicFile()
    }
    
    var mp3FileNames: Array<Any> = []
    var mp3FileUrl: Array<Any> = []
    var dismissAfterSongPick = false
    var delegate: allMusicDelegate? = nil


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        allmMsicFile()
    }
    
    //******* Get All Song Fine From The Document Folder
    func allmMsicFile() -> Void {
        // Get the document directory url
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            print(directoryContents)
            
            // if you want to filter the directory contents you can do like this:
            mp3FileUrl = directoryContents.filter{ $0.pathExtension == "mp3" }
            
            let mp3Files = directoryContents.filter{ $0.pathExtension == "mp3" }
            
            self.mp3FileNames = mp3Files.map{ $0.deletingPathExtension().lastPathComponent }
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    //********* Back Button
    @IBAction func goBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)

    }
    
    // MARK:- Table View Number  Section
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if mp3FileNames.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Music Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    // MARK:- Table View Number of Row in Section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mp3FileNames.count == 0 {
            tableView.separatorStyle = .none
        }
        return self.mp3FileNames.count
    }
    
    // MARK:- Table View Cell For Row At IndexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        
        tableView.separatorStyle = .singleLine
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        
        let data = mp3FileNames[indexPath.row]

        cell.textLabel?.text = data as? String
        
        return cell
    }
    
    // MARK:- Table View Did Select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let url = mp3FileUrl[indexPath.row]
        let name = mp3FileNames[indexPath.row]
        
        if dismissAfterSongPick == false {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "PostSongViewControllerID") as! PostSongViewController
            
            controller.songUrl = url as Any
            controller.songName = name as! String
            present(controller, animated: true, completion: nil)

        } else {
            self.dismiss(animated: true, completion: nil)
            delegate?.didFinishWith(songName: name as! String, songUrl: url as! URL)
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
