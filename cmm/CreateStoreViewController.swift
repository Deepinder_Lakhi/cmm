//
//  CreateStoreViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 26/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import Alamofire

class CreateStoreViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var chooseButon: UIButton!
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var genreTxtFld: UITextField!
    @IBOutlet weak var artistTxtFld: UITextField!
    @IBOutlet weak var prizeTxtFld: UITextField!
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var turnaroundTxtFld: UITextField!
    @IBOutlet weak var aboutTxt: UITextView!
    
    let prizePickerView = UIPickerView()
    let turnAroundPickerView = UIPickerView()
    
    var prizeOption = ["0 - 15", "15 - 30", "Over 30"]
    var turnAroundOption = ["Same day", "1 - 3 days", "More then 3 days"]
    
    @IBOutlet weak var containerView: DLViewExtender!

    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prizePickerView.delegate = self
        
        prizeTxtFld.inputView = prizePickerView
        
        turnAroundPickerView.delegate = self
        
        turnaroundTxtFld.inputView = turnAroundPickerView
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize = self.containerView.frame.size
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func openStoreBtnPressed(_ sender: Any) {
        let allObject = [emailTxtFld.text!, prizeTxtFld.text!, genreTxtFld.text!, aboutTxt.text!, artistTxtFld.text!]
        var success:Bool = false
        for object in allObject {
            if (object.isEmpty) {
                success = false
                break
            } else {
                success = true
            }
        }
        if success
        {
            openStore(imageView.image!)
        }
        else {
            print("Not yet")
            showAlert()
        }
    }
    
    @IBAction func btnClicked() {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        let selectedImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        // Set PhotoImageView to display the selected image
        imageView.image = selectedImage

        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func openStore(_ image: UIImage) {
        
        Global.sharedInstance.showLoaderWithMsg(self.view)

        let imageData = UIImageJPEGRepresentation(image, 1)
        
        let userID = UserDefaults.standard.string(forKey: "user_id")!

        let parameters = [
            "generes"         : "\(self.genreTxtFld.text!)",
            "custom_songs"    : "Music",
            "price_ranges"    : "\(self.prizeTxtFld.text!)",
            "about_artists"   : "\(aboutTxt.text!)",
            "user_id"         : "\(userID)",
            "turn_aroundtime" : "\(self.turnaroundTxtFld.text!)"
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "fileset",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:"\(baseUrl)creator_profile.php")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    DispatchQueue.main.async {
                        Global.sharedInstance.hideLoader()
                        self.moveToNextView()
                    }

                })
                
                upload.responseJSON { response in
                    print(response.result.value!)  
                }
                
            case .failure(let encodingError):
                print(encodingError)
                DispatchQueue.main.async {
                    Global.sharedInstance.hideLoader()
                    self.moveToNextView()
                }

            }
        }
    }
    
//    func myStoreOpenRequest()
//    {
//        
//        var myUrl:String = baseUrl
//        
//        myUrl.append("creator_profile.php")
//
//        
//        let request = NSMutableURLRequest(url: URL(string:myUrl)!);
//        request.httpMethod = "POST";
//        
//        let userID = UserDefaults.standard.string(forKey: "user_id")!
//        
//        let param = [
//            "generes"         : "\(self.genreTxtFld.text!)",
//            "custom_songs"    : "Music",
//            "price_ranges"    : "\(self.prizeTxtFld.text!)",
//            "about_artists"   : "\(aboutTxt.text!)",
//            "id"              : "\(userID)",
//            "turn_aroundtime" : "\(self.turnaroundTxtFld.text!)"
//        ]
//        
//        let boundary = generateBoundaryString()
//        
//        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//        
//        let imageData = UIImageJPEGRepresentation(imageView.image!, 1)
//        
//        if(imageData==nil)  { return; }
//        
//        request.httpBody = Global.sharedInstance.createBodyWithParameters(parameters: param, filePathKey: "file", imageDataKey: imageData! as NSData, boundary: boundary) as Data
//        
//        
//        let task = URLSession.shared.dataTask(with: request as URLRequest) {
//            data, response, error in
//            
//            if error != nil {
//                print("error=\(String(describing: error))")
//                return
//            }
//            
//            // You can print out response object
//            print("******* response = \(String(describing: response))")
//            
//            // Print out reponse body
//            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//            print("****** response data = \(responseString!)")
//            
//            do {
//                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
//                
//                print(json!)
//                
//                DispatchQueue.main.async {
//                    Global.sharedInstance.hideLoader()
//                    self.moveToMainView()
//                }
//
//            }catch
//            {
//                DispatchQueue.main.async {
//                    Global.sharedInstance.hideLoader()
//                    self.moveToMainView()
//                }
//
//                print(error)
//            }
//            
//        }
//        
//        task.resume()
//    }
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    
    
   /* func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-store-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        print(body)
        
        return body
    }
 */
    
    //MARK:- Image picker delegate
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == turnAroundPickerView {
            return turnAroundOption.count
        }
        return prizeOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == turnAroundPickerView {
            return turnAroundOption[row]
        }
        return prizeOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == turnAroundPickerView {
            turnaroundTxtFld.text = turnAroundOption[row]
        }
        else {
            prizeTxtFld.text = prizeOption[row]
        }
    }
    
    
    func moveToNextView() {
        let rootVC = storyboard!.instantiateViewController(withIdentifier: "MusicViewControllerID") as! MusicViewController
        
        UIApplication.shared.keyWindow?.rootViewController = rootVC
    }

    func showAlert() {
        let alertController = UIAlertController(title: "Please make sure all the fields are filled", message:"", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
    

