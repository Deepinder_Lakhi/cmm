//
//  CreatorsListsViewController.swift
//  MMC
//
//  Created by Prabhat on 18/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import Alamofire


protocol creatorListDelegate {
    func didFinishWith(data: NSMutableArray)
}

class CreatorsListsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate  {
    
    @IBOutlet var tableView: UITableView!
    var selectedMembers: NSMutableArray = []
    var isSearchActive = false
    var creatorID: String = ""
    var allData:Array<Any> = []
    var searchData:Array<Any> = []

    var downloadTask: URLSessionDownloadTask?
    
    var delegate: creatorListDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.hideKeyboardWhenTappedAround()
        test()
    }
    
    //UISearchBar delegates
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("DidBeginEditing")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("EndEditing")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.text = ""
        isSearchActive = false;
        searchBar.resignFirstResponder()
        print("CancelClicked")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false;
        loadSearchData(searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchText.characters.count == 0) {
            isSearchActive = false
            self.view.endEditing(true)
            self.tableView.reloadData()
        }
    }

    
    
    func loadSearchData(_ keyword:String) {
        var urlStr:String = baseUrl

        urlStr.append("Search_api_for_creatores.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let postString = "search_creators_name=\(keyword)"
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            print(jsonData!)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            let success  = json["Success"] as AnyObject
            
            if success as! String == "1" {
                
                self.isSearchActive = true
                self.searchData = json["creator_info"] as! Array
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
            
        }
        
        task.resume()
    }

    
    func test() -> Void {
        // Send HTTP GET Request
        Global.sharedInstance.showLoaderWithMsg(self.view)
        // Define server side script URL
        // Create NSURL Ibject
        let myUrl = NSURL(string: "\(baseUrl)send_inviataion_listofall_creators.php");
        
        // Creaste URL Request
        let request = NSMutableURLRequest(url:myUrl! as URL);
        
        // Set request HTTP method to GET. It could be POST as well
        request.httpMethod = "GET"
        
        // Excute HTTP Request
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            // Check for error
            if error != nil
            {
                print("error=\(String(describing: error))")
                Global.sharedInstance.hideLoader()
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            
            // Convert server json response to NSDictionary
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                                        
                    // Get value by key
                   let success = convertedJsonIntoDict["Success"] as? String
                    if success! == "1" {
                        self.allData = (convertedJsonIntoDict["craetors_lists"] as? NSArray as? Array<Any>)!
                        print(self.allData)
                        self.finishLoading()
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
                self.finishLoading()
            }
        }
        task.resume()
    }
    
    func finishLoading() {
        DispatchQueue.main.async {
            Global.sharedInstance.hideLoader()
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allData.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Creators Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive == true {
            return self.searchData.count
        } else {
            return self.allData.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreatorsListsCellID", for: indexPath) as! CreatorsListsCell
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Configure the cell...
        var allData = [String: Any]()

        if isSearchActive == true {
            allData = self.searchData[indexPath.row] as! [String: Any]

        } else {
            allData = self.allData[indexPath.row] as! [String: Any]
        }
        
        cell.name.text = (allData["user_name"] as? String)!
        cell.detailBtn.tag = indexPath.row;
        cell.detailBtn.addTarget(self, action: #selector(moveToStoreProfileOfUserWith(_:)), for: .touchUpInside)
        creatorID = (allData["id"] as? String)!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let cell = tableView.cellForRow(at: indexPath) {
//            let allData = self.allData[indexPath.row] as! [String: Any]
            var allData = [String: Any]()
            
            if isSearchActive == true {
                allData = self.searchData[indexPath.row] as! [String: Any]
                
            } else {
                allData = self.allData[indexPath.row] as! [String: Any]
            }
            let creatorID = allData["id"] as? String
            
            if cell.accessoryType == .checkmark {
                cell.accessoryType = .none
                // let cell = self.tableView.cellForRow(at: indexPath)
                self.selectedMembers.remove(creatorID as Any)
            }
            else {
                cell.accessoryType = .checkmark
                self.selectedMembers.add(creatorID as Any)
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
////        if let cell = tableView.cellForRow(at: indexPath) {
////            
////        }
//        var allData = [String: Any]()
//        if isSearchActive == true {
//            allData = self.searchData[indexPath.row] as! [String: Any]
//            
//        } else {
//            allData = self.allData[indexPath.row] as! [String: Any]
//        }
//
//        self.moveToStoreProfileOfUserWith(userID: (allData["id"] as? String)!)
//        
//    }
    
    func moveToStoreProfileOfUserWith(_ sender: UIButton) {
        let tag = sender.tag
        var allData = [String: Any]()
        if isSearchActive == true {
            allData = self.searchData[tag] as! [String: Any]
        } else {
            allData = self.allData[tag] as! [String: Any]
        }
        
        guard let userID = allData["id"] as? String else {
            return
        }
        
        let nextClassObj = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "musicStoreVC") as? MusicStoreViewController
        
        nextClassObj?.userID = userID
        nextClassObj?.titleText = "MUSIC STORE"
        
        self.show(nextClassObj!, sender: nil)
    }
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButton(_ sender: UIButton) {
            let data = selectedMembers
            delegate?.didFinishWith(data: data)
            dismiss(animated: true, completion: nil)
    }
}
