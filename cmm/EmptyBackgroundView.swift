//
//  EmptyBackgroundView.swift
//  EmptyTableView
//
//  Created by Ben Meline on 12/1/15.
//  Copyright © 2015 Ben Meline. All rights reserved.
//

import UIKit

class EmptyBackgroundView: UIView {
    
     var topSpace: UIView!
     var bottomSpace: UIView!
     var imageView: UIImageView!
     var topLabel: UILabel!
     var bottomLabel: UILabel!
    
     let topColor = UIColor.darkGray
     let topFont = UIFont.boldSystemFont(ofSize: 22)
     let bottomColor = UIColor.gray
     let bottomFont = UIFont.systemFont(ofSize: 18)
    
     let spacing: CGFloat = 10
     let imageViewHeight: CGFloat = 100
     let bottomLabelWidth: CGFloat = 300
    
    
    init(image: UIImage, top: String, bottom: String) {
        super.init(frame: CGRect.zero)
        //  setupImageView(image)
        setupLabels(top, bottom: bottom)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func setupImageView(_ image: UIImage) {
        imageView!.image = image
        imageView!.tintColor = topColor
    }
    
    func setupLabels(_ top: String, bottom: String) {
        print(bottom)

        topLabel.text = top
        topLabel.textColor = topColor
        topLabel.font = topFont
        
        bottomLabel?.text = bottom
        bottomLabel?.textColor = bottomColor
        bottomLabel?.font = bottomFont
        bottomLabel?.numberOfLines = 0
        bottomLabel?.textAlignment = .center
    }
}
