//
//  PaymentViewController.swift
//  
//
//  Created by AMAN SETHI on 15/11/17.
//
//

import UIKit

/*class PaymentViewController: UIViewController {
    
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var jobName: UILabel!
    @IBOutlet weak var jobPrice: UILabel!
    @IBOutlet weak var customerTitle: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var customerDetail: UILabel!
    
    var name: String? = ""
    var price: String? = ""
    var downloadTask: URLSessionDownloadTask?
    var priceValue: String  = ""
    var transactionID: String = ""
    var item: ListOfPostAppliedJobByCreatorView
    var storeID: String = ""
    var creatorID: String = ""
    var jobPostDetail: [String : AnyObject]?
    var jobPostDiscription: [String : AnyObject]?
    var UserInfo : [String: Any] = [:]
    var postId: String = ""


   //  var downloadTask: URLSessionDownloadTask?
    
    
    var environment:String = PayPalEnvironmentProduction {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        paypalConfig()
     //   updateUIData()
    }
    
   /* func updateUIData() -> Void {
      //  print(item?.jobPrice.uppercased() as Any)
        jobTitle.text = item.jobTitle.uppercaseFirst
     //   artistStore.text = item?.musicsArtistName.uppercaseFirst
        customerDetail.text = item.jobDesc.uppercaseFirst
     //   headerTitle.text = item?.musicsTitleTrack.uppercased()
        //        songDistance.setTitle(item?.musicsPric, for: .normal)
     //   musicType.setTitle(" Music Type: " + (item?.musicsType.uppercaseFirst)!, for: .normal)
        
        jobPrice.text = "$" + priceValue + ".00"
    }*/
    
    func paypalConfig() -> Void {
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "MMC, Inc."
        
        // These URLs are just PayPal merchant policy
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.acceptCreditCards = true
        
        // This is the language with which the PayPal ios sdk will be showed to the user. We use zero as the default language of the device.
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        // If you use the paypal, it will use the address which is registered in paypal. Howeverif you use both, the customer will have the choice to use a diffrent ddress or the one in Paypal. We are going to go for both
        payPalConfig.payPalShippingAddressOption = .none;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    
    
    
    @IBAction func backBtn() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func payNowButton(_ sender: Any) {
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        // Here are the some item that are being sold for example
        // sku is probably the item id provided by the merchant
        let item = PayPalItem(name: (self.item.jobTitle)!, withQuantity: 1, withPrice: NSDecimalNumber(string: priceValue), withCurrency: "USD", withSku: "Hip-0037")
        
        let items = [item]
        
        // The Total Price
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "00")
        let tax = NSDecimalNumber(string: "00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        // The Total Price with tax and shipping
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: (self.item?.musicsTitleTrack)!, intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self as! PayPalPaymentDelegate)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
    }
    
    func jobPayment() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("consumer_payment_screen.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "Payment"
        let consumerId = UserDefaults.standard.string(forKey: "user_id")
        let creatorId = UserInfo["id"] as! String
        
        var postString = "creator_id=\(creatorId)&"
        postString.append("post_id=\(postId)&")
        postString.append("consumer_id=\(consumerId!)")
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                //                self.alertMessage(title: "Success!", message: "")
            }
        }
        
        task.resume()

        
    }
    // PayPalPaymentDelegate
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            // print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.resultText = completedPayment.description
            
            // print(completedPayment.description)
            
            let paymentResultDic = completedPayment.confirmation as NSDictionary
            
            let dicResponse: AnyObject? = paymentResultDic.object(forKey: "response") as AnyObject?
            
            self.transactionID = dicResponse!["id"] as! String
            print(self.transactionID)
            
            self.getUserProfileData()
            
        })
    }
       /*
     transaction_id
     store_id
     craetor_id
     buyer_id
     paypal_email
     total_amonut
     */
    
    
    /* Helper Method To POST The Data On Server */
    func getUserProfileData() -> Void {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("/payment_success_return.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")!
        
        storeID = (item.musicsStoreId)!
        creatorID = (item.musicsid)!
        
        
        var postString = "transaction_id=\(transactionID)&"
        postString.append("store_id=\(storeID)&")
        postString.append("craetor_id=\(creatorID)&")
        postString.append("buyer_id=\(userID)&")
        // postString.append("paypal_email=\()&")
        postString.append("total_amonut=\(priceValue)")
        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
            }
        }
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}

}*/
