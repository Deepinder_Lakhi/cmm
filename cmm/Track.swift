//
//  trackswift
//  Swift Radio
//
//  Created by Matthew Fecher on 7/2/15.
//  Copyright (c) 2015 MatthewFecher.com. All rights reserved.
//

import UIKit

//*****************************************************************
// Track struct
//*****************************************************************

struct Track {
    var id: NSNumber
	var artistName: String = ""
    var imageURL: String = ""
    var artworkImage = UIImage(named: "albumArt")
    var musicType:String = ""
    var price: NSNumber
    var storeID: NSNumber
    var titleTrack:String = ""
    var artworkLoaded:Bool? = false
    var isPlaying: Bool? = false
}
