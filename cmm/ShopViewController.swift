//
//  ShopViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 23/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import CoreAudio

class ShopViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AVAudioPlayerDelegate, UISearchBarDelegate {
   

    @IBOutlet weak var musicStoreBtn: DLButtonExtender!
    
    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var instrumentalMusicBtn: DLButtonExtender!
    var stations = [RadioStation]()
    var searchedStations = [RadioStation]()
    var currentStation: RadioStation?
    var currentTrack: Track?
    var firstTime = true
    var isSearchActive = false
    
    
    var userInfo = NSArray()
    var track: Track!
    var currentTag:Int?
    var downloadTask: URLSessionDownloadTask?
    
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "shopCell"
    
    let stopWatch = StopWatch()
    
    var paidSongData = NSArray()
    
    @IBOutlet weak var searchBtn: UIButton!
    
    @IBOutlet var tabBar: UITabBar!
    
    // don't forget to hook this up from the storyboard
    @IBOutlet var tableView: UITableView!
    
//    let isSearchActive = false
//    var searchData:Array<Any> = []

    var player: AVPlayer!
    var observer: Any!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        callApi(with: "Musical")
        instrumentalMusicBtn.titleLabel?.textColor = UIColor.white
    }
    
    @IBAction func togglSearchBtn(_ sender: Any) {
        if (sender as! UIButton).imageView?.image == UIImage.init(named: "cancel-music") {
            (sender as! UIButton).setImage(UIImage.init(named: "search"), for: .normal)
            searchBarTopConstraint.constant = 74
            isSearchActive = false
            self.view.endEditing(true)
            self.finishLoading()
            self.currentTag = self.stations.count

        } else {
            (sender as! UIButton).setImage(UIImage.init(named: "cancel-music"), for: .normal)
            searchBarTopConstraint.constant = 30
        }
    }
    //UISearchBar delegates
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("DidBeginEditing")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        print("EndEditing")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
//        searchBar.text = ""
//        isSearchActive = false
        self.view.endEditing(true)
        print("CancelClicked")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false;
        loadSearchData(searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchText.characters.count == 0) {
//            isSearchActive = false
            self.view.endEditing(true)
//            self.tableView.reloadData()
        }
    }
    
    func loadSearchData(_ keyword:String) {
        
        var urlStr:String = baseUrl
        
        urlStr.append("search_api_all_songs.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")

        let postString = "search_values=\(keyword)&user_id=\(String(describing: userID!))"
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            guard let jsonData = responseString?.data(using: .utf8) else { return }
            let json = JSON(data: jsonData as Data)
            print(json)
            if let stationArray = json["search_values"].array {
                self.searchedStations = []
                for stationJSON in stationArray {
                    let station = RadioStation.parseStation(stationJSON: stationJSON)
                    self.searchedStations.append(station)
                    if let stationArray = json["search_values_new"].array {
                        for obj in stationArray {
                            let store_id = obj["store_id"].string ?? ""
                            if store_id == stationJSON["store_id"].string ?? "" {
                                
                            }
                        }
                    }
                }
                
                
                print(self.searchedStations)
                // stations array populated, update table on main queue
                DispatchQueue.main.async(execute: {
                    self.currentTag = self.searchedStations.count
                    self.isSearchActive = true
                    self.tableView.reloadData()
                    self.view.setNeedsDisplay()
                })
            } else {
                if kDebugLog { print("JSON Station Loading Error") }
            }

        }
        task.resume()
    }

    
    override func viewWillLayoutSubviews() {
        self.view.bringSubview(toFront: searchBtn)
    }

    
    @IBAction func changeType(_ sender: UIButton) {
        stations = []
        if sender.tag == 103 {
            
            //Setup the instrumental selected
            instrumentalMusicBtn.backgroundColor = UIColor.mmcBlue()
            instrumentalMusicBtn.setTitleColor(UIColor.white, for: .normal)
            
            musicStoreBtn.backgroundColor = UIColor.clear
            musicStoreBtn.setTitleColor(UIColor.mmcBlue(), for: .normal);
            callApi(with: "Instrumental")
        } else {
            
            //Setup the Music selected
            instrumentalMusicBtn.backgroundColor = UIColor.clear
            instrumentalMusicBtn.setTitleColor(UIColor.mmcBlue(), for: .normal);
            
            musicStoreBtn.backgroundColor = UIColor.mmcBlue()
            musicStoreBtn.setTitleColor(UIColor.white, for: .normal);

            callApi(with: "Musical")
        }
    }
    
    
    
    func getUserStore() -> Void {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("/creator_stores_fetch.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let userID = UserDefaults.standard.string(forKey: "user_id")

        let postString = "id=\(userID!)"
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            print(json)
            
            self.userInfo = json["users_details"] as! NSArray
            print(self.userInfo)
        
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.view.setNeedsDisplay()
            }
        }
        task.resume()
    }
    func callApi(with type:String) {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        APIManager.getListOfMusicsStation(type: type ,{
            (details: Data) in
            
            let responseString = String(data: details, encoding: .utf8)
            print("responseString",responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let jsonObj = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            print(jsonObj)
            
            
            if let obj = jsonObj["shop_history_new"] {
                self.paidSongData = obj as! NSArray
            }
            
            
            let json = JSON(data: details as Data)
            
            print("All Shop data\(json)")

            if let stationArray = json["shop_history"].array {
                for stationJSON in stationArray {
                    print("Data to copy: \(stationJSON)")
                    let station = RadioStation.parseStation(stationJSON: stationJSON)
                    self.stations.append(station)
                }
                print(self.stations)
                // stations array populated, update table on main queue
                DispatchQueue.main.async(execute: {
                    self.currentTag = self.stations.count
                    self.tableView.reloadData()
                    self.view.setNeedsDisplay()
//                     self.getUserStore()
                })
            } else {
                if kDebugLog { print("JSON Station Loading Error") }
            }
            
            // Turn off network indicator in status bar
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.finishLoading()
        })
    }
    
    func finishLoading() {
        DispatchQueue.main.async {
            Global.sharedInstance.hideLoader()
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func resetPlayer() {
        //Here we are reserting the player for the next use
        player.pause()
        self.currentTag = self.stations.count + 1
        self.tableView.reloadData()
    }
    
    func resumePlayer() {
        //Here we are reserting the player for the next use
        player.play()
        self.currentTag = self.stations.count + 1
        self.tableView.reloadData()
    }

    
    @IBAction func play(_ sender: UIButton) {
        let tag = sender.tag
        if tag == currentTag {
            sender.isSelected = !sender.isSelected
            if(sender.imageView?.image == UIImage(named: "icon-play")) {
                sender.setImage(UIImage(named: "icon-pause"), for: UIControlState.normal)
                player.play()
            } else {
                sender.setImage(UIImage(named: "icon-play"), for: UIControlState.normal)
                player.pause()
            }
        } else {
            currentTag = tag
            tableView.reloadData()
        }
    }
    
    func playSong(url:URL, isBoundryNeeded:Bool) {
        player = AVPlayer(url: url)
        player.play()
        if isBoundryNeeded == true{
            let boundary: TimeInterval = 10
            let times = [NSValue(time: CMTimeMake(Int64(boundary), 1))]
            observer = player.addBoundaryTimeObserver(forTimes: times, queue: nil) {
                [weak self] time in
                print("10s reached")
                if let observer = self?.observer {
                    self?.player.removeTimeObserver(observer)
                }
                self?.resetPlayer()
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if stations.count > 0 {
            self.tableView.backgroundView = nil
            numOfSection = 1
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "No Uploads Yet!"

            if isSearchActive == true {
                noDataLabel.text = "No Results Found!"
            }
            noDataLabel.textColor = UIColor(red: 16.0/255.0, green: 194.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive ==  true {
            return searchedStations.count
        } else {
            return stations.count
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.backgroundColor = UIColor.white
        self.view.endEditing(true)
        // create a new cell if needed or reuse an old one
        //let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ShopTableViewCell
        cell.selectionStyle = .none
        
        // Configure the cell...
        var station = stations[indexPath.row]
        if isSearchActive ==  true {
            station = searchedStations[indexPath.row]
        }
        
        cell.songTitleHead.text = station.musicsTitleTrack
        cell.artistText.text = station.musicsArtistName
        print(" Music Type: \(station.musicsType)")
        print(" Duration: \(station.musicsDuration)")
//        cell.genreText.titleLabel?.text = " Music Type: \(station.musicsType)"
        cell.genreText.setTitle(" Music Type: \(station.musicsType)", for: .normal)
        cell.durationText.setTitle(" Duration: \(station.musicsDuration)", for: .normal)
        cell.playBtn.tag = indexPath.row
        
        let imageURL = station.musicsImage as String
        
        if imageURL.contains("http") {
            
            if let url = URL(string: station.musicsImage) {
                downloadTask = cell.thumbImageView.loadImageWithURL(url: url) { (image) in
                    // station image loaded
                }
            }
            
        } else if imageURL != "" {
            cell.thumbImageView.image = UIImage(named: imageURL as String)
        } else {
            cell.thumbImageView.image = UIImage(named: "stationImage")
        }
        
        
        var isPaidStation = false
        for obj in paidSongData {
            let songDetail  = obj as! [String:Any]
            let getPaidStation = songDetail["store_id"] as! String
            if getPaidStation == station.musicsStoreId {
                isPaidStation = true
            }
        }
        
        var isBoundryNeeded = true
        let userID = UserDefaults.standard.string(forKey: "user_id")
        
        if isPaidStation {
            print("Owned the song @\(indexPath.row)")
            cell.buyButton.isHidden = true
            cell.price.text = "Already Purchased"
            isBoundryNeeded = false
        }
        else {
            if userID == station.musicsid {
                print("uploaded the song @\(indexPath.row)")
                cell.buyButton.isHidden = true
                cell.price.text = "My song"
                isBoundryNeeded = false
            } else {
                print("Song @\(indexPath.row)")
                cell.price.text = "$ " + station.musicsPrice
                cell.buyButton.addTarget(self, action: #selector(self.selectedRadioStation), for: .touchUpInside)
                cell.buyButton.tag = indexPath.row
                isBoundryNeeded = true
            }
        }

        

        if indexPath.row == currentTag {
            cell.playBtn.setImage(UIImage(named: "icon-pause"), for: UIControlState.normal)
            
            let musicURL = station.musicsMusicFile as String

            self.playSong(url: URL(string:musicURL)!, isBoundryNeeded: isBoundryNeeded)
        } else {
            cell.playBtn.setImage(UIImage(named: "icon-play"), for: UIControlState.normal)
        }
        cell.artistInfo.addTarget(self, action: #selector(self.artistStore), for: .touchUpInside)
        cell.artistInfo.tag = indexPath.row
        return cell
    }
    
    func artistStore(_ sender: AnyObject) {
        print(sender.tag)
        let userID = stations[sender.tag].musicsid
        let nextClassObj = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "musicStoreVC") as? MusicStoreViewController
        print(userID)
        nextClassObj?.userID = userID
        nextClassObj?.titleText = "MUSIC STORE"
        
        self.show(nextClassObj!, sender: nil)
    }
    
    /* Buy Button */
    func selectedRadioStation(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SongDetailViewControllerID") as! SongDetailViewController
        let station = stations[sender.tag]
        controller.item = station as RadioStation
      //  controller.creatorID = stations
        present(controller, animated: true, completion: nil)
    }
    

}
