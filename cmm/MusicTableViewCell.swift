//
//  MusicTableViewCell.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 27/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit


class MusicTableViewCell: UITableViewCell {

    @IBOutlet var userImg: UIImageView!
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var SongDur: UIButton! 
    @IBOutlet var songType: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet var buyButton: UIButton!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
       
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
