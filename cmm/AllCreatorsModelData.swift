//
//  AllCreatorsModelData.swift
//  MMC
//
//  Created by Prabhat on 18/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class AllCreatorsModelData: NSObject {
    
    var creatorId  :String
    var userName :String
    var UserImg  :String

    
    init(id: String, user_name: String, user_profile_image: String) {
        self.creatorId = id
        self.userName  = user_name
        self.UserImg   = user_profile_image

    }
    
    //*****************************************************************
    // MARK: - JSON Parsing into object
    //*****************************************************************
    
    class func parseStation(stationJSON: JSON) -> (AllCreatorsModelData) {
        
        let creatorId           = stationJSON["id"].string ?? ""
        let user_name           = stationJSON["user_name"].string ?? ""
        let user_profile_image  = stationJSON["user_profile_image"].string ?? ""
        
        let allCreators = AllCreatorsModelData(id: creatorId, user_name: user_name, user_profile_image: user_profile_image)
        
        return allCreators
    }
    
}

