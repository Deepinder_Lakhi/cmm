//
//  SignupViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 09/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    /**
     * This we are using for understanding that user in creator or consumer
     *
     * If it's value is true then is means user is a creator or on other hand consumer.
     *
     **/
    
    let titles:[String] = ["Request Custom Songs & Profit or Download Free & Paid Songs.", "Request Custom Songs & Profit or Download Free & Paid Songs."]
    
    @IBOutlet weak var containerView: DLViewExtender!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAbout: UITextField!
    @IBOutlet var scroll: UIScrollView!

    @IBOutlet weak var titleHeader: UILabel!
    
    var response = [String:AnyObject]()
    var success: String!
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        if Global.sharedInstance.user_role == "creator" {
            titleHeader.text = titles[0]
            txtName.placeholder = "Artist Name"
        } else {
            titleHeader.text = titles[1]
            txtName.placeholder = "Customer Name"
            txtName.placeHolderColor = .white
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.pop(true)
    }
    
    @IBAction func signUpButtonPressed()
    {
        
        print(txtPassword)
        
        let allObject = [txtName.text!, txtAbout.text!, txtPassword.text!, txtConfirmPassword.text!, txtEmail.text!]
        var success:Bool = false
        for object in allObject {
            if (object.isEmpty) {
                success = false
                break
            } else {
                success = true
            }
        }
        
        if success {
            if txtPassword.text! == txtConfirmPassword.text! {
                callSignUp()
            } else {
                alertMessage(title: "Confirm Password Should Be Same.", message: "")
            }
        }
        else {
            alertMessage(title: "Please make sure all the fields are filled.!", message: "")
        }
        

    }
    
    func callSignUp()
    {
        
        Global.sharedInstance.showLoaderWithMsg(self.view)
        
        var urlStr:String = baseUrl
        
        if Global.sharedInstance.user_role == "creator" {
            urlStr.append("creator_signup.php")
        } else {
            urlStr.append("consumer_signup.php")
        }
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        var postString = "first_name=\(txtName.text!)&"
        postString.append("users_email=\(txtEmail.text!)&")
        postString.append("users_password=\(txtPassword.text!)&")
        postString.append("password=\(txtConfirmPassword.text!)&")
        postString.append("about_me=\(txtAbout.text!)")
        
        /*
        
       
         $_POST['pin_code'];
         $_POST['address'];
         $_POST['wallet_nfo'];
         $_POST['about_me'];
         */
        
//        postString.append("custom_songs_made=\("")&")
//        postString.append("price_range=\("")&")
//        postString.append("about_artist=\("")&")
//        postString.append("turn_around_time=\("")")
//        postString.append("file_image_upload=\("")&")
//        postString.append("paypal_account=\("")&")
//        postString.append("country=\("")&")
//        postString.append("state=\("")")
//        postString.append("pin_code=\("")")
//        postString.append("address=\("")")
//        postString.append("wallet_nfo=\("")")
//        postString.append("about_me=\("")")

        
        request.httpBody = postString.data(using: .utf8)
        
        print(postString)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            let jsonData = responseString?.data(using: .utf8)
            var json = try? JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json as Any)
            
                let success = json?["Success"] as! String
                
                if success == "1" {
                    let defaults = UserDefaults.standard
                    
                    defaults.set(json, forKey: "userProfile")
                    
                    defaults.set(json?["id"], forKey: "user_id")
                    defaults.set(json?["user_role"], forKey: "user_type")
                }
            
        DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
                    if success == "1" {
                        self.moveToNextView()
                    } else {
                    self.alertMessage(title: "User Already Exist.!", message: "")
                    }
                }
            }
        
        task.resume()
    }
    
    func moveToNextView() {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ProfileSetupViewControllerID") as! ProfileSetupViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    /*
     *@param: Title And Message Are The String Parameter
     *@return: Void
     *@des: Alert Message Function
     */
    
    func alertMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Ok")
        })
        alert.addAction(dismissButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
