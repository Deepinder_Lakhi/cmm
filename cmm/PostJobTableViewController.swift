//
//  PostJobTableViewController.swift
//  MMC
//
//  Created by Prabhat on 29/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PostJobTableViewController: UITableViewController, UITextFieldDelegate,  creatorListDelegate, IndicatorInfoProvider {

    var creatorCount = 0
    @IBOutlet var creatorCountLbl: UILabel!
    @IBOutlet var creatorLbl: UILabel!
    /* UI Objects */
    @IBOutlet weak var songName: UITextField!
    @IBOutlet weak var genreOptional: UITextField!
    @IBOutlet weak var lenght: UITextField!
    @IBOutlet weak var songContents: UITextView!
    @IBOutlet weak var budget: UITextField!
    
    var creatorsList: Array<Any> = []
    
    /* View Did Load Method */
    override func viewDidLoad() {
        super.viewDidLoad()
        creatorCountLbl.text = String(creatorCount)
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismissKeyboard()
    }
    
    //Delegate Method
    func didFinishWith(data: NSMutableArray) {
        creatorsList = data as! Array<Any>
        print(creatorsList)
        let size = creatorsList.count
        creatorCount = size
        creatorCountLbl.text = String(creatorCount)
        print(creatorCountLbl.text!)
    }
    
    /*
     *@des: To Show The Tab Name
     */
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "POST")
    }
    
    
    
    @IBAction func goToLeaderBoard(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LeaderBoardViewControllerID") as! LeaderBoardViewController
        present(controller, animated: true, completion: nil)

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true)
    }

    @IBAction func inviteCreatorButton(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CreatorsListsViewControllerID") as! CreatorsListsViewController
        controller.delegate = self
        present(controller, animated: true, completion: nil)
    }
    
    /* Post Button */
    @IBAction func postRequest(_ sender: Any) {
        if songName.text == "" || genreOptional.text == "" || budget.text == "" || lenght.text == "" || songContents.text == "" {
            errorMessage(title: "Error", message: "Please Fill The All fields")
        } else {
            postJob()
            songName.text = nil
            genreOptional.text = nil
            budget.text = nil
            lenght.text = nil
            songContents.text = nil
            creatorCountLbl.text = "0"
        }
    }
    
    
    /* Helper Method To POST The Data On Server */
    func postJob() {
        var urlStr:String = baseUrl
        urlStr.append("/Consumer_search.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")        
        var postString = "id=\(userID!)&"
        postString.append("title=\(songName.text!)&")
        postString.append("genre=\(genreOptional.text!)&")
        postString.append("budget=\(budget.text!)&")
        postString.append("length=\(lenght.text!)&")
        postString.append("song_content=\(songContents.text!)&")
        postString.append("creator_id=\(creatorsList)")
        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                self.removeLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                self.removeLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            DispatchQueue.main.async {
                self.removeLoader()
                self.errorMessage(title: "Success", message: "Request Added Successfuly!")
            }
        }
        
        task.resume()
    }
    
    /*
     *@param: Title And Message Are The String Parameter
     *@return: Void
     *@des: Alert Message Function
     */
    func errorMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        alert.addAction(dismissButton)
        self.present(alert, animated: true, completion: nil)
    }
 
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func removeLoader() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveLoader"), object: nil, userInfo: nil)
    }
    
}
