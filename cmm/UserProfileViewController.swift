//
//  UserProfileViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 18/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import Alamofire

class UserProfileViewController: UITableViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var myStoreBtn: DLButtonExtender!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var centerImage: UIImageView!
    @IBOutlet weak var aboutDesc: UITextView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var favouritePlaylist: UILabel!
    @IBOutlet weak var peopleFollowLbl: UILabel!
    @IBOutlet weak var walletLbl: UILabel!
    @IBOutlet weak var paypalID: UILabel!
    @IBOutlet weak var emailID: UILabel!
    
    var name: String? = ""
    var Img: String! = ""
    var userAbout: String? = ""
    var userAddress: String? = ""
    var favourtePlaylist: String? = ""
    var pepleFollow: String? = ""
    var wallet: String? = ""
    var UserPaypalID: String? = ""
    var userEmailID: String? = ""
    
    var downloadTask: URLSessionDownloadTask?
    var imagePicker = UIImagePickerController()
    let userID = UserDefaults.standard.string(forKey: "user_id")

    override var prefersStatusBarHidden: Bool {
        return true
    }

    func myStoreBtnPressed(_ sender: Any) {
        guard let nav = self.storyboard?.instantiateViewController(withIdentifier: "musicStoreVC") as! MusicStoreViewController!
            else {
            return
        }
        nav.titleText = "MY STORE"
        nav.isUploadNeeded = true
        nav.userID = UserDefaults.standard.string(forKey: "user_id")!
        self.present(nav, animated: true, completion: nil)
    }
    
    func mySongsBtnPressed(_ sender: Any) {
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "MySongsViewControllerID") as! MySongsViewController!
        self.present(nav!, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        getUserProfileData()
        
        // Initialization code
        centerImage.layer.masksToBounds = false
        centerImage.layer.cornerRadius = centerImage.frame.height/2
        
        centerImage.clipsToBounds = true
        
    }
    
    override func viewWillLayoutSubviews() {
        if Global.sharedInstance.user_role == "consumer" {
            myStoreBtn.setTitle("My Songs", for: .normal)
            myStoreBtn.addTarget(self, action: #selector(self.mySongsBtnPressed(_:)), for: .touchUpInside)
        } else {
            myStoreBtn.setTitle("My Store", for: .normal)
            myStoreBtn.addTarget(self, action: #selector(self.myStoreBtnPressed(_:)), for: .touchUpInside)
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUserProfileData()
    }
    
    
    @IBAction func editProfileButton(_ sender: DLButtonExtender) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "ProfileEditViewControllerID") as! ProfileEditViewController
        present(controller, animated: true, completion: nil)
        
    }
    
    func postWithDrawRequest() {
        var urlStr:String = baseUrl
        
        urlStr.append("withdraw_api_for_users.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        var postString = "users_id=\(String(describing: userID!))&"
        postString.append("wallet_amount=\(String(describing: self.wallet!))")
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            print(jsonData!)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            let success  = json["Success"] as AnyObject
            
            if success as! String == "1" {
                DispatchQueue.main.async {
                    let message  = json["message"] as AnyObject
                    Global.sharedInstance.alertMessage(title: "Success!", message: message as! String, mySelf: self)
                }
            } else {
                DispatchQueue.main.async {
                    let message  = json["message"] as AnyObject
                    Global.sharedInstance.alertMessage(title: "Alert!", message: message as! String, mySelf: self)
                }
            }
//
                
        }
        
        task.resume()
    }

    
    @IBAction func withdraw() {
        self.postWithDrawRequest()
    }
    
    /* Helper Method To POST The Data On Server */
    func getUserProfileData() -> Void {
        
        var urlStr:String = baseUrl
        
        urlStr.append("/login_user_data.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        
        let postString = "id=\(userID!)"
        
        print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                self.removeLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                self.removeLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
           // let details = json["details"] as! [String: AnyObject]
            let success = json["Success"] as! String
            
            if (success == "1") {
                
                let stationArray = json["users_details"] as! NSArray
                
                let allData  = stationArray[0] as! [String:AnyObject]
                
                print(allData)
                
                print((allData["user_profile_image"] as? String) as Any)
                
                self.name = allData["user_name"] as? String
                self.Img = allData["user_profile_image"] as? String
                let img = allData["user_profile_image"] as? String
                if img != nil {
                    let fileUrl = Foundation.URL(string: img!)
                    if fileUrl != nil {
                        print(fileUrl!)
                        self.downloadImage(url: fileUrl!)
                    }
                }

                /* self.userAbout = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi u"
                 
                 */
                
                self.userAbout =  allData["about_me"] as? String
                self.userAddress = "\(String(describing: allData["Address"] as! String)), \(String(describing: allData["State"] as! String)), \(String(describing: allData["Country"] as! String))"
                self.wallet = allData["wallet_nfo"] as? String
                self.UserPaypalID = allData["Paypal_account"] as? String
                self.userEmailID = allData["user_email"] as? String
                
                if self.userAbout == nil {
                    self.userAbout = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi u"
                }
                if self.userAddress == nil {
                     self.userAddress = "USA"
                }
                if self.UserPaypalID == nil{
                    self.UserPaypalID = "paypal@gmai.com"
                }
            }
            
            DispatchQueue.main.async {
                self.removeLoader()
                self.showDataInUIObjects()
            }
        }
        
        task.resume()
    }
    
    
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        
      //  Global.sharedInstance.showLoaderWithMsg(self.view)
        
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            
            print("Download Finished")
            DispatchQueue.main.async() {
               
                () -> Void in
                //Global.sharedInstance.hideLoader()
                self.centerImage.image = UIImage(data: data)
                self.bgImage.image = UIImage(data: data)
                              self.removeLoader()

                

            }
        }
    }
    
    func showDataInUIObjects() -> Void {
        self.userName.text = self.name
        self.aboutDesc.text = self.userAbout
        self.address.text = self.userAddress
        self.walletLbl.text = "Wallet $ \(String(describing: self.wallet!))"
        self.paypalID.text = self.UserPaypalID
        self.emailID.text = self.userEmailID
    }
    
 

    @IBAction func updateImage(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        let selectedImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
//        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        
        // Set PhotoImageView to display the selected image
        bgImage.image = selectedImage
        centerImage.image = selectedImage
        
        print(selectedImage)
        uploadImageByURL(selectedImage)
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func uploadImageByURL(_ image: UIImage) -> Void {
        
        //Global.sharedInstance.showLoaderWithMsg(self.view)
        
        let imageData = UIImageJPEGRepresentation(image, 1)
        
        let userID = UserDefaults.standard.string(forKey: "user_id")!
        
        let parameters = [
            "id" : "\(userID)",
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "profile_images",fileName: "file.jpg", mimeType: "image/jpg")
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:"\(baseUrl)user_profile_image.php")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    if progress.fractionCompleted == 1.0 {
                        DispatchQueue.main.async {
//                            Global.sharedInstance.hideLoader()
                        }
                    }
                    
                })
                
                upload.responseJSON { response in
                    print(response.result.value!)
                    DispatchQueue.main.async {
                        self.removeLoader()
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                DispatchQueue.main.async {
                   self.removeLoader()
                }
                
            }
        }
    }
    
    func removeLoader() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveLoader"), object: nil, userInfo: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "info" {
            let nextScene =  segue.destination as! WebShellViewController
            nextScene.myTitle = "How it works"
            // Pass the selected object to the new view controller.
            nextScene.urlString = "http://musicmarketcorrection.com/webapp/apps-profit.php"
        }
        if segue.identifier == "terms" {
            let nextScene =  segue.destination as! WebShellViewController
            nextScene.myTitle = "Terms & Conditions"
            // Pass the selected object to the new view controller.
            nextScene.urlString = "http://musicmarketcorrection.com/webapp/terms-and-condition.php"
        }
        if segue.identifier == "privacy" {
            let nextScene =  segue.destination as! WebShellViewController
            nextScene.myTitle = "Privacy Policy"
            // Pass the selected object to the new view controller.
            nextScene.urlString = "http://musicmarketcorrection.com/webapp/privacy-policy.php"
        }
        
    }

    
}
