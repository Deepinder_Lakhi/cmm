//
//  LaunchViewController.swift
//  MMC
//
//  Created by Maninder on 01/03/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.pop(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "termsText" {
            let nextScene =  segue.destination as! WebShellViewController
            nextScene.myTitle = "Terms & Conditions"
            // Pass the selected object to the new view controller.
            nextScene.urlString = "\(baseUrl)terms-and-condition.php"
        }
    }


}

