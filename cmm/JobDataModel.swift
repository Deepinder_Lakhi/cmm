//
//  JobDataModel.swift
//  MMC
//
//  Created by Prabhat on 28/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class JobDataModel: NSObject {
    
    var postId      :String
    var consumerId  :String
    var title       :String
    var genre       :String
    var budget      :String
    var length      :String
    var songContent :String
    var status      :String
    
    init(post_id: String, consumer_id: String, title: String, genre: String, budget: String, length: String, song_content: String, status: String) {
        
        self.postId       = post_id
        self.consumerId   = consumer_id
        self.title        = title
        self.genre        = genre
        self.budget       = budget
        self.length       = length
        self.songContent  = song_content
        self.status       = status
    }
    
    //*****************************************************************
    // MARK: - JSON Parsing into object
    //*****************************************************************
    
    class func parseStation(stationJSON: JSON) -> (JobDataModel) {
        
        let post_id       = stationJSON["post_id"].string ?? ""
        let consumer_id   = stationJSON["consumer_id"].string ?? ""
        let title         = stationJSON["title"].string ?? ""
        let genre         = stationJSON["genre"].string ?? ""
        let budget        = stationJSON["budget"].string ?? ""
        let length        = stationJSON["length"].string ?? ""
        let song_content  = stationJSON["song_content"].string ?? ""
        let status        = stationJSON["status"].string ?? ""
        
        
        let jobData = JobDataModel(post_id: post_id, consumer_id: consumer_id, title: title, genre: genre, budget: budget, length: length, song_content: song_content, status: status)
        
        return jobData
    }

}
