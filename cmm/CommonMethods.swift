//
//  CommonMethods.swift
//  MMC
//
//  Created by Prabhat on 25/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class CommonMethods: NSObject {
    
    

}

//extension UINavigationController {
//    var rootViewController : UIViewController? {
//        return viewControllers.first as? UIViewController
//    }
//}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}



extension UIColor {
    
    static func mmcBlue() -> UIColor {
        return UIColor(red:   37/255.0,
                       green: 173/255.0,
                       blue: 230/255.0,
                       alpha: 1.0)
        
         
    }
    
}

