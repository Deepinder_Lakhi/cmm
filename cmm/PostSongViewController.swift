//
//  PostSongViewController.swift
//  MMC
//
//  Created by Prabhat on 23/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation

class PostSongViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   
    var songUrl: Any?
    var songName: String = ""
    var imagePicker = UIImagePickerController()
    var type = "Musical"
    
    @IBOutlet weak var containerView: DLViewExtender!
    @IBOutlet weak var radio_one: DLButtonExtender!
    @IBOutlet weak var radio_two: DLButtonExtender!
    
    @IBOutlet var nameField: UITextField!
    @IBOutlet var artistName: UITextField!
    @IBOutlet var priceRange: UITextField!
    @IBOutlet var musicType: UITextField!
    @IBOutlet var musicLenght: UITextField!
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var songDesc: UITextView!
    @IBOutlet var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameField.text = songName
        self.vocal_select(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scroll.contentSize = self.containerView.frame.size

    }
    

    @IBAction func goBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chooseImage(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        // Set PhotoImageView to display the selected image
        imageView.image = selectedImage
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func vocal_select(_ sender: Any) {
        radio_one.image = UIImage(named: "radio_check")
        radio_two.image = nil
        type = "Musical"
        priceRange.text = ""
        
    }
    @IBAction func inst_select(_ sender: Any) {
        radio_one.image = nil
        radio_two.image = UIImage(named: "radio_check")
        type = "Instrumental"
        priceRange.isUserInteractionEnabled = true
        priceRange.text = "0.15"

    }

    @IBAction func postSong(_ sender: DLButtonExtender) {
        let allObject = [nameField.text!, artistName.text!, priceRange.text!, musicType.text!]
        var success:Bool = false
        for object in allObject {
            if (object.isEmpty) {
                success = false
                break
            } else {
                success = true
            }
        }
        if success
        {
            postSong()
        }
        else {
            print("Not yet")
            errorMessage(title: "Error", message: "Please Fill The All fields")
        }
    }
    
    
    
    var player: AVAudioPlayer?
    
    func playSound() {
        let url = songUrl as! URL
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    func postSong() {
        
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var songData:Data?
        
        let imageData = UIImageJPEGRepresentation(imageView.image!, 1)
        
        do {
            songData = try NSData(contentsOf: songUrl as! URL, options: NSData.ReadingOptions()) as Data
            print(songData!)
        } catch {
            print(error)
        }
        
        let userID = UserDefaults.standard.string(forKey: "user_id")!
        
        
        //-->Add the two category:-
//        Musical
//        Instrumental

        let parameters = [
            "track_title"      : "\(self.nameField.text!)",
            "artist_name"      : "\(self.artistName.text!)",
            "type_of_music"    : "\(self.musicType.text!)",
            "price"            : "\(self.priceRange.text!)",
            "length"           : "\(self.musicLenght.text!)",
            "song_desc"        : "\(self.songDesc.text!)",
            "id"               : "\(userID)",
            "catogery_songs"   : "\(type)"
        ]
        
        print(parameters)
        
        let timeStamp = Date().ticks
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(songData!, withName: "file_uploads",fileName: "\(timeStamp)_\(self.songName).mp3", mimeType: "music/mp3")
            multipartFormData.append(imageData!, withName: "image_uploads",fileName: "\(timeStamp)_\(self.songName).jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:"\(baseUrl)creator_shop_page.php")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    if progress.fractionCompleted == 1.0 {
                        DispatchQueue.main.async {
                            Global.sharedInstance.hideLoader()
                            self.clearTextFields()
                        }
                    }
                })
                
                upload.responseJSON { response in
                   // print(response.result.value as Any)
                }
                
            case .failure(let encodingError):
                print(encodingError)
                DispatchQueue.main.async {
                    Global.sharedInstance.hideLoader()
                }
                
            }
        }
    }
    
    
    func clearTextFields() -> Void {
        nameField.text = nil
        artistName.text = nil
        priceRange.text = nil
        musicType.text = nil
        dismiss(animated: true, completion: nil)
    }
    
    
    /*
     *@param: Title And Message Are The String Parameter
     *@return: Void
     *@des: Alert Message Function
     */
    func errorMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        alert.addAction(dismissButton)
        self.present(alert, animated: true, completion: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
