//
//  JobsContainerViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 20/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class JobsContainerViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global.sharedInstance.showLoaderWithMsg(self.view)
        self.perform(#selector(self.hide), with: nil, afterDelay: 0.3)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func hide() {
        Global.sharedInstance.hideLoader()
        if Global.sharedInstance.user_role == "creator" {
            loadCreatorJobsView()
        } else {
            loadCustomerJobsView()
        }
    }

    func loadCreatorJobsView() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "JobsCreatorViewControllerID") as! JobsCreatorViewController
        self.addChildViewController(viewController)
        viewController.view.frame = containerView.bounds
        containerView.addSubview(viewController.view)
        
    }

    func loadCustomerJobsView() {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "JobsConsumerViewControllerID") as! JobsConsumerViewController
        self.addChildViewController(viewController)
        viewController.view.frame = containerView.bounds
        containerView.addSubview(viewController.view)
        
    }
    
    

}
