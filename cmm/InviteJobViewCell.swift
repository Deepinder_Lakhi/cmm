
//
//  InviteJobViewCell.swift
//  MMC
//
//  Created by Prabhat on 23/05/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit

class InviteJobViewCell: UITableViewCell {

    @IBOutlet var img: UIImageView!
    @IBOutlet var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
