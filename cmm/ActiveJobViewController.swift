//
//  ActiveJobViewController.swift
//  MMC
//
//  Created by Prabhat on 29/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ActiveJobViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    @IBOutlet var tableView: UITableView!

    var allHiredJob:Array<Any> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        /*
        tableView.estimatedRowHeight = 96.0
        tableView.rowHeight = UITableViewAutomaticDimension
        */
    }
    
    
    @IBAction func completeJobPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "JobCompletionViewControllerID") as! JobCompletionViewController
        controller.isCustomer = true
        
        let cellData = allHiredJob[(sender as AnyObject).tag] as! [String: Any]

        print(cellData)
        
        controller.consumer_id = cellData["consumer_id"] as! String
        controller.creator_id = cellData["creator_id"] as! String
        controller.post_id = cellData["post_id"] as! String
        controller.post_title = cellData["title"] as! String
        
        present(controller, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewWillLayoutSubviews() {
        getHiredJob()
    }
    
    func showDetails(sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "JobDetailCreatorViewController") as! JobDetailCreatorViewController
        let jobData = allHiredJob[sender.tag] as! [String:Any]
        controller.userInfo = jobData
        present(controller, animated: true, completion: nil)
    }

    
    /* Helper Method To GET The Data On Server */
    func getHiredJob() {
        var urlStr:String = baseUrl
        urlStr.append("consumer_hired_activejob.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")
        let postString = "consumer_id=\(userID!)"
        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                self.removeLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                self.removeLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            // print(self.userInfoNil!)
            let success = json["Success"] as? String
            if success! == "1" {
                self.allHiredJob = (json["job_hire"] as! NSArray) as! Array<Any>
                print(self.allHiredJob)
                self.removeLoader()

            }
            
            
            DispatchQueue.main.async {
                self.removeLoader()
                self.tableView.reloadData()
            }
        }
        
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "ACTIVE")
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allHiredJob.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            noDataLabel.text = "No Active Job Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allHiredJob.count
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//    
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        removeLoader()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActiveJobCellID", for: indexPath) as! ActiveJobCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let cellData = allHiredJob[indexPath.row] as! [String: Any]
        
        cell.jobTitle.text = cellData["title"] as? String
        cell.jobDesc.text = cellData["description"] as? String
        
        let price = cellData["budget"] as? String
        cell.jobPrice.text = "$ " + price!
        cell.completeBtn.tag = indexPath.row

        let status = cellData["status"] as? String
        
        cell.jobStatus.text = status?.uppercaseFirst
        cell.fullView.tag = indexPath.row
        cell.fullView.addTarget(self, action: #selector(self.showDetails(sender:)), for: .touchUpInside)

        return  cell
    }
    
    func removeLoader() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveLoader"), object: nil, userInfo: nil)
    }

}
