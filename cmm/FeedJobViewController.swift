//
//  FeedJobViewController.swift
//  MMC
//
//  Created by Prabhat on 30/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class FeedJobViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!
    var allData:Array<Any> = []
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var jobDataModel = [JobDataModel]()
    
    // don't forget to hook this up from the storyboard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Custom Table View Cell Identifier
        tableView.register(UINib(nibName:"JobTableViewCell", bundle: nil), forCellReuseIdentifier: "JobTableViewID")
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if jobDataModel.isEmpty {
            getAllFeed()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismissKeyboard()
    }
    
    func getAllFeed() -> Void {
        APIManager.getListOfJobData({
            (details: [String:Any]) in
            
            let success  = details["success"] as! Int
            
            if success == 1 {
                self.allData = (details["result"] as? NSArray as? Array<Any>)!
                     //print(self.allData)
                    // stations array populated, update table on main queue
                    self.finishLoading()
                
            } else {
                DispatchQueue.main.async {
                   self.removeLoader()
                }
            }
            
        })
    }

    
    func finishLoading() {
        DispatchQueue.main.async {
            self.removeLoader()
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
    
    /*
     *@des: To Show The Tab Name
     */
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "FEED")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger = 0
        
        if allData.count > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))

            noDataLabel.text = "No Jobs Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.font = UIFont(name: "HelveticaNeue", size: 20)

            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }

        
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
     var applyId: String!
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        removeLoader()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobTableViewID", for: indexPath) as! JobTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Configure the cell...
        let jobData = allData[indexPath.row] as! [String: Any]
        cell.jobStatus.tag = indexPath.row
    
        
        cell.songTitle.text = jobData["title"] as? String
        cell.songContent.text = jobData["song_content"] as? String
        
        let g = jobData["genre"] as? String
        
        cell.genre.text = "Genre:- " + g!
        let price = jobData["budget"] as? String
        cell.budgetPrice.text = "$ " + price!
        
        let length = jobData["length"] as? String
        
        cell.songLength.text = "Length:- " + length!
        
//        let s = jobData["status"] as? String
//        cell.timeAgo.text = "Status:- " + (s?.uppercaseFirst)!
        
        let time = jobData["time"] as? String
        
        cell.timeAgo.text = time

        cell.jobStatus.addTarget(self, action: #selector(FeedJobViewController.selected(sender:)), for: .touchUpInside)
        
        let postId = jobData["post_id"] as? String

        cell.jobStatus.tag = Int(postId!)!

        return cell
    }
    
    func selected(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ApplyNowViewControllerID") as! ApplyNowViewController
        controller.applyId = String(sender.tag)
        
        present(controller, animated: true, completion: nil)
    }

    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func removeLoader() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveLoader"), object: nil, userInfo: nil)
    }
    
}
