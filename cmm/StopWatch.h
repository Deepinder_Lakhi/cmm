//
//  StopWatch.h
//  stopwatch
//
//  Created by DEEPINDERPAL SINGH on 25/07/17.
//  Copyright © 2017 Kevin Kelly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol StopWatchProtocol
- (void)updateLabel:(NSString *)value;
@end


@interface StopWatch : NSObject
{
    NSTimer *clockTicks;
    NSDate *start_time;
    NSDate *pause_time;
    NSDate *current_time;
}

@property (nonatomic, weak) id <StopWatchProtocol> delegate;


- (void)startPressed:(id)sender;
- (void)pausePressed:(id)sender;
- (void)resumePressed:(id)sender;
- (void)stopPressed:(id)sender;

//@property (weak, nonatomic) UILabel *secondsLabel;
@property (weak, nonatomic) UIButton *startStop;
@property (weak, nonatomic) UIButton *pauseResume;





//@property (weak, nonatomic) IBOutlet UILabel *secondsLabel;
//@property (weak, nonatomic) IBOutlet UIButton *startStop;
//@property (weak, nonatomic) IBOutlet UIButton *pauseResume;


@end
