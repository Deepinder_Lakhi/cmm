//
//  NowPlayingViewController.swift
//  MMC
//
//  Created by Prabhat on 27/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import MediaPlayer

//*****************************************************************
// Protocol
// Updates the StationsViewController when the track changes
//*****************************************************************

protocol NowPlayingViewControllerDelegate: class {
    func songMetaDataDidUpdate(track: Track)
    func artworkDidUpdate(track: Track)
    func trackPlayingToggled(track: Track)
}


class NowPlayingViewController: UIViewController {
    
    @IBOutlet weak var albumImageView: SpringImageView!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var songLabel: SpringLabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var volumeParentView: UIView!
    
    var currentStation: RadioStation!
    
    var downloadTask: URLSessionDownloadTask?
    var iPhone4 = false
    var justBecameActive = false
    var newStation = true
    var nowPlayingImageView: UIImageView!
    let radioPlayer = Player.radio
    var track: Track!
    var mpVolumeSlider = UISlider()
    
    weak var delegate: NowPlayingViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup MPMoviePlayerController
        // If you're building an app for a client, you may want to
        // replace the MediaPlayer player with a more robust
        // streaming library/SDK. Preferably one that supports interruptions, etc.
        // Most of the good streaming libaries are in Obj-C, however they
        // will work nicely with this Swift code. There is a branch using RadioKit if
        // you need an example of how nicely this code integrates with libraries.
        setupPlayer()
        
        
        // Notification for when app becomes active
        NotificationCenter.default.addObserver(self,selector: #selector(NowPlayingViewController.didBecomeActiveNotificationReceived),name: Notification.Name("UIApplicationDidBecomeActiveNotification"),object: nil)
        
        // Notification for MediaPlayer metadata updated
        NotificationCenter.default.addObserver(self,selector: #selector(NowPlayingViewController.metadataUpdated),name: Notification.Name.MPMoviePlayerTimedMetadataUpdated,object: nil)
        
        // Notification for AVAudioSession Interruption (e.g. Phone call)
        NotificationCenter.default.addObserver(self, selector: #selector(NowPlayingViewController.sessionInterrupted), name: Notification.Name.AVAudioSessionInterruption, object: AVAudioSession.sharedInstance())
 
        // Check for station change
        if newStation {
//            track = Track()
            stationDidChange()
        } else {
            updateLabels()
            albumImageView.image = track.artworkImage
        
            if !track.isPlaying! {
//                pausePressed()
            } else {
                nowPlayingImageView.startAnimating()
            }
        }
    
        // Setup slider
        //        setupVolumeSlider()
    }
    
    func didBecomeActiveNotificationReceived() {
        // View became active
//        updateLabels()
        justBecameActive = true
//        updateAlbumArtwork()
    }
    
    deinit {
        // Be a good citizen
        NotificationCenter.default.removeObserver(self,
        name: Notification.Name("UIApplicationDidBecomeActiveNotification"),
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
        name: Notification.Name.MPMoviePlayerTimedMetadataUpdated,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
        name: Notification.Name.AVAudioSessionInterruption,
                                                  object: AVAudioSession.sharedInstance())
    }

    
    //*****************************************************************
    // MARK: - Setup
    //*****************************************************************
    
    func setupPlayer() {
        radioPlayer.view.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        radioPlayer.view.sizeToFit()
        radioPlayer.movieSourceType = MPMovieSourceType.streaming
        radioPlayer.isFullscreen = false
        radioPlayer.shouldAutoplay = true
        radioPlayer.prepareToPlay()
        radioPlayer.controlStyle = MPMovieControlStyle.none
    }
    
    func setupVolumeSlider() {
        // Note: This slider implementation uses a MPVolumeView
        // The volume slider only works in devices, not the simulator.
        volumeParentView.backgroundColor = UIColor.clear
        let volumeView = MPVolumeView(frame: volumeParentView.bounds)
        for view in volumeView.subviews {
            let uiview: UIView = view as UIView
            if (uiview.description as NSString).range(of: "MPVolumeSlider").location != NSNotFound {
                mpVolumeSlider = (uiview as! UISlider)
            }
        }
        
        let thumbImageNormal = UIImage(named: "slider-ball")
        slider?.setThumbImage(thumbImageNormal, for: .normal)
        
    }
    
    func stationDidChange() {
//        radioPlayer.stop()
//        
//        radioPlayer.contentURL = URL(string: currentStation.musicsMusicFile)
//        radioPlayer.prepareToPlay()
//        radioPlayer.play()
        
//        updateLabels(statusMessage: "Loading Station...")
        
        // songLabel animate
//        songLabel.animation = "flash"
//        songLabel.repeatCount = 3
//        songLabel.animate()
        
        resetAlbumArtwork()
        
//        track.isPlaying = true
    }
    
    
    //*****************************************************************
    // MARK: - Player Controls (Play/Pause/Volume)
    //*****************************************************************
    
    @IBAction func pause(_ sender: UIButton) {
        print("Pressed")
        
//        playButtonEnable()
        
        radioPlayer.pause()
        //        updateLabels(statusMessage: "Station Paused...")
//        nowPlayingImageView.stopAnimating()
        
        // Update StationsVC
//        self.delegate?.trackPlayingToggled(track: self.track)
    }
    
    var isPlaying:Bool = true
    
    @IBAction func play(_ sender: UIButton) {
        
        if isPlaying{
            print("play")
            radioPlayer.play()
        }else {
            radioPlayer.pause()
        }
    }
    //*****************************************************************
    // MARK: - UI Helper Methods
    //*****************************************************************
    
    func playButtonEnable(enabled: Bool = true) {
        if enabled {
            playButton.isEnabled = true
            pauseButton.isEnabled = false
            track.isPlaying = false
        } else {
            playButton.isEnabled = false
            pauseButton.isEnabled = true
            track.isPlaying = true
        }
    }
    
    func updateLabels(statusMessage: String = "") {
        
        if statusMessage != "" {
            // There's a an interruption or pause in the audio queue
//            songLabel.text = statusMessage
            artistLabel.text = currentStation.musicsArtistName
            
        } else {
            // Radio is (hopefully) streaming properly
            if track != nil {
                songLabel.text = track.titleTrack
                artistLabel.text = track.artistName
            }
        }
    
        // Hide station description when album art is displayed or on iPhone 4
        /*
         if track.artworkLoaded || iPhone4 {
         stationDescLabel.isHidden = true
         } else {
         stationDescLabel.isHidden = false
         stationDescLabel.text = currentStation.stationDesc
         }
         */
    }
    
    func startNowPlayingAnimation() {
        nowPlayingImageView.startAnimating()
    }
    
    //*****************************************************************
    // MARK: - Album Art
    //*****************************************************************
    
    func resetAlbumArtwork() {
//        track.artworkLoaded = false
        //track.image = currentStation.musicsImage
        updateAlbumArtwork()
    }
    
    
    func updateAlbumArtwork() {
//        track.artworkLoaded = false
        if track.imageURL.range(of: "http") != nil {
            
            // Attempt to download album art from an API
            if let url = URL(string: track.imageURL) {
                
                self.downloadTask = self.albumImageView.loadImageWithURL(url: url) { (image) in
                    
                    // Update track struct
                    self.track.artworkImage = image
                    self.track.artworkLoaded = true
                    
                    // Turn off network activity indicator
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    // Animate artwork
                    self.albumImageView.animation = "wobble"
                    self.albumImageView.duration = 2
                    self.albumImageView.animate()
                    
                    // Update lockscreen
                    self.updateLockScreen()
                    
                    // Call delegate function that artwork updated
                    self.delegate?.artworkDidUpdate(track: self.track)
                }
            }
            
        } else if track.imageURL != "" {
            // Local artwork
            self.albumImageView.image = UIImage(named: track.imageURL)
            track.artworkImage = albumImageView.image
            
            // Call delegate function that artwork updated
            self.delegate?.artworkDidUpdate(track: self.track)
            
        } else {
            // No Station or API art found, use default art
            self.albumImageView.image = UIImage(named: "albumArt")
            track.artworkImage = albumImageView.image
        }
        
        // Force app to update display
        self.view.setNeedsDisplay()
    }
    
    //*****************************************************************
    // MARK: - MPNowPlayingInfoCenter (Lock screen)
    //*****************************************************************
    
    func updateLockScreen() {
        
        // Update notification/lock screen
        let albumArtwork = MPMediaItemArtwork(image: track.artworkImage!)
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [
            MPMediaItemPropertyArtist: track.artistName,
            MPMediaItemPropertyTitle: track.titleTrack,
            MPMediaItemPropertyArtwork: albumArtwork
        ]
    }
    
    override func remoteControlReceived(with receivedEvent: UIEvent?) {
        super.remoteControlReceived(with: receivedEvent)
        
        if receivedEvent!.type == UIEventType.remoteControl {
            
            switch receivedEvent!.subtype {
            case .remoteControlPlay: break
//                playPressed()
            case .remoteControlPause: break
//                pausePressed()
            default:
                break
            }
        }
    }

    
    //*****************************************************************
    // MARK: - MetaData Updated Notification
    //*****************************************************************
    
    func metadataUpdated(n: NSNotification)
    {
        if(radioPlayer.timedMetadata != nil && radioPlayer.timedMetadata.count > 0)
        {
            startNowPlayingAnimation()
            
            let firstMeta: MPTimedMetadata = radioPlayer.timedMetadata.first as! MPTimedMetadata
            let metaData = firstMeta.value as! String
            
            var stringParts = [String]()
            if metaData.range(of: " - ") != nil {
                stringParts = metaData.components(separatedBy: " - ")
            } else {
                stringParts = metaData.components(separatedBy: "-")
            }
            
            // Set artist & songvariables
            let currentSongName = track.titleTrack
            track.artistName = stringParts[0]
            track.titleTrack = stringParts[0]
            
            if stringParts.count > 1 {
                track.titleTrack = stringParts[1]
            }
            
            if track.artistName == "" && track.titleTrack == "" {
                track.artistName = currentStation.musicsArtistName
                track.titleTrack = currentStation.musicsTitleTrack
            }
            
            DispatchQueue.main.async(execute: {
                
                if currentSongName != self.track.titleTrack {
                    
                    if kDebugLog {
                        print("METADATA artist: \(self.track.artistName) | title: \(self.track.titleTrack)")
                    }
                    
                    // Update Labels
                    self.artistLabel.text = self.track.artistName
                    self.songLabel.text = self.track.titleTrack
                    self.updateUserActivityState(self.userActivity!)
                    
                    // songLabel animation
//                    self.songLabel.animation = "zoomIn"
//                    self.songLabel.duration = 1.5
//                    self.songLabel.damping = 1
//                    self.songLabel.animate()
                    
                    // Update Stations Screen
                    self.delegate?.songMetaDataDidUpdate(track: self.track)
                    
                    // Query API for album art
                    self.resetAlbumArtwork()
                    self.updateLockScreen()
                    
                }
            })
        }
    }
    //*****************************************************************
    // MARK: - AVAudio Sesssion Interrupted
    //*****************************************************************
    
    // Example code on handling AVAudio interruptions (e.g. Phone calls)
    func sessionInterrupted(notification: NSNotification) {
        if let typeValue = notification.userInfo?[AVAudioSessionInterruptionTypeKey] as? NSNumber{
            if let type = AVAudioSessionInterruptionType(rawValue: typeValue.uintValue){
                if type == .began {
                    print("interruption: began")
                    // Add your code here
                } else{
                    print("interruption: ended")
                    // Add your code here
                }
            }
        }
    }


    
}
