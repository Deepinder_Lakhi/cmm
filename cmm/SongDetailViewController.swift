//
//  SongDetailViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 19/04/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import PassKit

class SongDetailViewController: UIViewController, PayPalPaymentDelegate {
    
    @IBOutlet weak var paymentPopup: UIView!
    @IBOutlet var songImage: UIImageView!
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var artistName: UILabel!
    @IBOutlet var songDuration: UIButton!
    @IBOutlet var songDesc: UITextView!
    @IBOutlet var musicType: UIButton!
    @IBOutlet var songPrice: UILabel!
    @IBOutlet var headerTitle: UILabel!
    
    var name: String? = ""
    var price: String? = ""
    var item: RadioStation?
    var downloadTask: URLSessionDownloadTask?
    var priceValue: String  = ""
    var transactionID: String = ""
    var storeID: String = ""
    var creatorID: String = ""
    

    var environment:String = PayPalEnvironmentProduction {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }

    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for character in (item?.musicsPrice.characters)!{
            print(character)
            if character != "$" {
                priceValue.append(character)
            }
        }
        
        print(priceValue)
        
        paypalConfig()
        updateUIData()
    }
    
    func moveToStoreProfileOfUserWith() {
        
//        guard let userID = allData["id"] as? String else {
//            return
//        }
        
        let nextClassObj = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "musicStoreVC") as? MusicStoreViewController
        
        nextClassObj?.userID = item?.musicsid
        //print("UserID before fetching the store data : \(String(describing: nextClassObj?.userID))")
        nextClassObj?.titleText = "MUSIC STORE"
        self.show(nextClassObj!, sender: nil)
    }
    
    
    func updateUIData() -> Void {
        print(item!)
        songTitle.text = item?.musicsTitleTrack.uppercaseFirst
        artistName.text = item?.musicsArtistName.uppercaseFirst
        songDesc.text = item?.musicsArtistName.uppercaseFirst
        headerTitle.text = item?.musicsTitleTrack.uppercased()
//        songDistance.setTitle(item?.musicsPric, for: .normal)
        musicType.setTitle(" Music Type: " + (item?.musicsType.uppercaseFirst)!, for: .normal)
        
        songPrice.text = "$" + priceValue
        
        let imageURL = item?.musicsImage
        
        if (imageURL?.contains("http"))! {
            
            if let url = URL(string: (item?.musicsImage)!) {
                downloadTask = songImage.loadImageWithURL(url: url) { (image) in
                    // station image loaded
                }
            }
            
        } else if imageURL != "" {
            songImage.image = UIImage(named: imageURL!)
        } else {
            songImage.image = UIImage(named: "stationImage")
        }
    }
    
    func paypalConfig() -> Void {
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "MMC, Inc."
        
        // These URLs are just PayPal merchant policy
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.acceptCreditCards = true
        
        // This is the language with which the PayPal ios sdk will be showed to the user. We use zero as the default language of the device.
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        // If you use the paypal, it will use the address which is registered in paypal. Howeverif you use both, the customer will have the choice to use a diffrent ddress or the one in Paypal. We are going to go for both
        payPalConfig.payPalShippingAddressOption = .none;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        paymentPopup.isHidden = true
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    
    
    @IBAction func artistStore(_ sender: UIButton) {
        let userID = item?.musicsid
        
        let nextClassObj = Global.sharedInstance.storyboard.instantiateViewController(withIdentifier: "musicStoreVC") as? MusicStoreViewController
        
        nextClassObj?.userID = userID
        nextClassObj?.titleText = "MUSIC STORE"
        
        self.show(nextClassObj!, sender: nil)
    }
    
    @IBAction func backBtn() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func payNowButton(_ sender: Any) {
        paymentPopup.isHidden = false
    }
    
    @IBAction func cancelPayNow(_ sender: Any) {
        paymentPopup.isHidden = true
    }
    
    
    
    @IBAction func payUsingPaypal() {
        paymentPopup.isHidden = true
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        // Here are the some item that are being sold for example
        // sku is probably the item id provided by the merchant
        let item = PayPalItem(name: (self.item?.musicsTitleTrack)!, withQuantity: 1, withPrice: NSDecimalNumber(string: priceValue), withCurrency: "USD", withSku: "Hip-0037")
        
        let items = [item]
        
        // The Total Price
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "00")
        let tax = NSDecimalNumber(string: "00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        // The Total Price with tax and shipping
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: (self.item?.musicsTitleTrack)!, intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            print("Payment not processable: \(payment)")
        }
    }
    
    let SupportedPaymentNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
    let ApplePaySwagMerchantID = "merchant.com.bayview.cmm" // Fill in your merchant ID here!
    let request = PKPaymentRequest()

    
    @IBAction func payUsingApplePay() {
        paymentPopup.isHidden = true
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: SupportedPaymentNetworks) {
            request.countryCode = "US"
            request.currencyCode = "USD"
            request.merchantIdentifier = ApplePaySwagMerchantID
            request.supportedNetworks = SupportedPaymentNetworks
            request.merchantCapabilities = PKMerchantCapability.capability3DS
            request.paymentSummaryItems = [
                PKPaymentSummaryItem(label: (self.item?.musicsTitleTrack)!, amount: NSDecimalNumber(string: priceValue))
            ]

            let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
            applePayController.delegate = self
            self.present(applePayController, animated: true, completion: nil)
        }
    }
    
    // PayPalPaymentDelegate
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
           // print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.resultText = completedPayment.description
            
           // print(completedPayment.description)
            
            let paymentResultDic = completedPayment.confirmation as NSDictionary
            
            let dicResponse: AnyObject? = paymentResultDic.object(forKey: "response") as AnyObject?
            
            self.transactionID = dicResponse!["id"] as! String
            print(self.transactionID)
            
            self.getUserProfileData()

        })
    }
    
    /*
    transaction_id
    store_id
    craetor_id
    buyer_id
    paypal_email
    total_amonut
    */
    
    
    /* Helper Method To POST The Data On Server */
    func getUserProfileData() -> Void {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        var urlStr:String = baseUrl
        urlStr.append("/payment_success_return.php")
        
        var request = URLRequest(url: URL(string:urlStr)!)
        
        request.httpMethod = "POST"
        let userID = UserDefaults.standard.string(forKey: "user_id")!
        
        storeID = (item?.musicsStoreId)!
        creatorID = (item?.musicsid)!
        
        
        var postString = "transaction_id=\(transactionID)&"
        postString.append("store_id=\(storeID)&")
        postString.append("craetor_id=\(creatorID)&")
        postString.append("buyer_id=\(userID)&")
       // postString.append("paypal_email=\()&")
        postString.append("total_amonut=\(priceValue)")
        
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
                Global.sharedInstance.hideLoader()
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                Global.sharedInstance.hideLoader()
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            let jsonData = responseString?.data(using: .utf8)
            
            let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! [String:Any]
            
            print(json)
            
            DispatchQueue.main.async {
                Global.sharedInstance.hideLoader()
            }
        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SongDetailViewController: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: ((PKPaymentAuthorizationStatus) -> Void)) {
        completion(PKPaymentAuthorizationStatus.success)
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
//    private func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController!, didSelectShippingAddress address: ABRecord!, completion: ((PKPaymentAuthorizationStatus, [AnyObject]!, [AnyObject]!) -> Void)!) {
//        completion(status: PKPaymentAuthorizationStatus.Success, shippingMethods: nil, summaryItems: nil)
//    }
}
