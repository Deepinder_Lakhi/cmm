//
//  JobCompletionViewController.swift
//  MMC
//
//  Created by DEEPINDERPAL SINGH on 12/07/17.
//  Copyright © 2017 Digiwolves. All rights reserved.
//

import UIKit
import Alamofire


class JobCompletionViewController: UIViewController, allMusicDelegate {

    
    var songUrl: URL?
    var songName: String = ""
    var post_id: String = ""
    var post_title: String = ""
    var consumer_id: String = ""
    var creator_id: String = ""

    var isCustomer: Bool = false
    
    @IBOutlet weak var starRatingView: HCSStarRatingView!
    
    @IBOutlet weak var feedBackTxtView: UITextView!
    @IBOutlet weak var uploadSectionHeightzConstraint: NSLayoutConstraint!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        if Global.sharedInstance.user_role == "consumer" {
            uploadSectionHeightzConstraint.constant = 0
        }
    }
    
    //********* Back Button
    @IBAction func goBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doGo (_ sender: Any!) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AllMusicsFilesViewControllerID") as! AllMusicsFilesViewController
        controller.delegate = self
        controller.dismissAfterSongPick = true
        present(controller, animated: true, completion: nil)
    }
    
    func didFinishWith(songName: String, songUrl: URL) {
        print(songName)
        self.songUrl = songUrl
        self.songName = songName
    }
    
    @IBAction func submitBtnPressed() {
        if (feedBackTxtView.text?.isEmpty)!
        {
            self.alertMessage(title: "Alert", message: "Please write something in feedback")
        } else {
            if Global.sharedInstance.user_role == "consumer" {
                self.postCustomerReview()
            } else {
                self.postSong()
            }
        }
    }
    
    func alertMessage(title:String, message:String)->Void {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let dismissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Ok")
        })
        alert.addAction(dismissButton)
        self.present(alert, animated: true, completion: nil)
    }

    
    //http://musicmarketcorrection.com/webapp/consumer_list_songs_by uploading_creators.php
    
    func postCustomerReview() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        /*
         $post_id = $_POST['post_id'];
         $consumer_id = $_POST['consumer_id'];
         $craetor_id = $_POST['craetor_id'];
         $feedback = $_POST['feedback'];
         $rating = $_POST['rating'];
         $post_title = $_POST['post_title'];
         */
        
        let rating = starRatingView.value
        
        let parameters = [
            "post_id"      : "\(post_id)",
            "consumer_id"      : "\(consumer_id)",
            "craetor_id"      : "\(creator_id)",
            "feedback"      : "\(feedBackTxtView.text!)",
            "rating"    : String(describing: rating),
            "post_title"      : "\(post_title)"
        ]
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:"\(baseUrl)consumer_job_completion_rating_to_creator.php")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    if progress.fractionCompleted == 1.0 {
                        DispatchQueue.main.async {
                            Global.sharedInstance.hideLoader()
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                })
                
                upload.responseJSON { response in
                    // print(response.result.value as Any)
                }
                
            case .failure(let encodingError):
                print(encodingError)
                DispatchQueue.main.async {
                    Global.sharedInstance.hideLoader()
                }
                
            }
        }
    }
    
    func postSong() {
        Global.sharedInstance.showLoaderWithMsg(self.view)
        if (songUrl != nil) {
            var songData:Data?
            do {
                songData = try NSData(contentsOf: songUrl!, options: NSData.ReadingOptions()) as Data
                print(songData!)
            } catch {
                print(error)
            }
            
            let rating = starRatingView.value
            
            
            let parameters = [
                "post_id"      : "\(post_id)",
                "feedback"      : "\(feedBackTxtView.text!)",
                "rating"    : String(describing: rating)
            ]
            print(parameters)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(songData!, withName: "upload_song",fileName: "final.mp3", mimeType: "music/mp3")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }, to:"\(baseUrl)upload_song_job_completion.php")
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                        if progress.fractionCompleted == 1.0 {
                            DispatchQueue.main.async {
                                Global.sharedInstance.hideLoader()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    })
                    
                    upload.responseJSON { response in
                        // print(response.result.value as Any)
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    DispatchQueue.main.async {
                        Global.sharedInstance.hideLoader()
                    }
                    
                }
            }
         }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
